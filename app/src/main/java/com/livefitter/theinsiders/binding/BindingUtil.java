package com.livefitter.theinsiders.binding;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.widget.ImageView;
import android.widget.TextView;

import com.livefitter.theinsiders.R;
import com.squareup.picasso.Picasso;

import jp.wasabeef.picasso.transformations.GrayscaleTransformation;

/**
 * Created by LloydM on 1/30/17
 * for Livefitter
 */

public class BindingUtil {
    /**
     * Takes a url and uses Picasso to asynchronously load the image into a given imageView
     * @param imageView
     * @param url
     */
    @BindingAdapter("app:imageSrc")
    public static void setImageUrl(ImageView imageView, String url){
        if(url != null && !url.isEmpty()) {
            Picasso.with(imageView.getContext()).load(url).into(imageView);
        }
    }

    /**
     * Takes a uri and uses Picasso to asynchronously load the image into a given imageView
     * @param imageView
     * @param uri
     */
    @BindingAdapter("app:imageSrc")
    public static void setImageUri(ImageView imageView, Uri uri){
        if(uri != null) {
            Picasso.with(imageView.getContext()).load(uri).into(imageView);
        }
    }

    /**
     * Takes a drawable and uses Picasso to asynchronously load the image into a given imageView
     * @param imageView
     * @param drawable
     */
    @BindingAdapter("app:imageSrc")
    public static void setImageDrawable(ImageView imageView, Drawable drawable){
        if(drawable != null) {
            imageView.setImageDrawable(drawable);
        }
    }

    /**
     * Takes a resourceId and uses Picasso to asynchronously load the image into a given imageView
     * @param imageView
     * @param resourceId
     */
    @BindingAdapter("app:imageSrc")
    public static void setImageResource(ImageView imageView, int resourceId){
        if(resourceId != 0) {
            Picasso.with(imageView.getContext()).load(resourceId).into(imageView);
        }
    }

    /**
     * Takes a url and uses Picasso to asynchronously load the image into a given imageView
     * and applies {@link GrayscaleTransformation} on the image
     * @param imageView
     * @param url
     */
    @BindingAdapter("app:imageSrcTransformationGrayscale")
    public static void setImageUrlGrayScaleTransformation(ImageView imageView, String url){
        if(url != null && !url.isEmpty()) {
            Picasso.with(imageView.getContext()).load(url)
                    .transform(new GrayscaleTransformation())
                    .into(imageView);
        }
    }

    @BindingAdapter({"bind:font"})
    public static void setFont(TextView textView, String fontName){
        textView.setTypeface(Typeface.createFromAsset(textView.getContext().getAssets(), "fonts/" + fontName));
    }

    @BindingAdapter({"bind:font"})
    public static void setFont(TextView textView, @StringRes int resourceIdFontName){
        if(resourceIdFontName != 0) {
            Context context = textView.getContext();
            String fontName = context.getString(resourceIdFontName);
            if(!fontName.isEmpty()) {
                textView.setTypeface(Typeface.createFromAsset(textView.getContext().getAssets(), "fonts/" + fontName));
            }
        }
    }

    @BindingAdapter("app:setHtmlText")
    public static void setHtmlText(TextView textView, String message){

        textView.setLinksClickable(true);
        textView.setLinkTextColor(ContextCompat.getColor(textView.getContext(), R.color.text_gold));
//        textView.setText(Html.fromHtml(message));
        Linkify.addLinks(textView, Linkify.ALL);
        textView.setMovementMethod(LinkMovementMethod.getInstance());

    }
}
