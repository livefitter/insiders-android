package com.livefitter.theinsiders.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.livefitter.theinsiders.databinding .ItemHomeEventBinding;
import com.livefitter.theinsiders.listener.EventClickListener;
import com.livefitter.theinsiders.model.EventModel;

import java.util.ArrayList;


/**
 * Created by LloydM on 1/30/17
 * for Livefitter
 */

public class HomeEventPagerAdapter extends PagerAdapter {

    // You can choose a bigger number for LOOPS, but you know, nobody will fling
    // more than 1000 times just in order to test your "infinite" ViewPager :D
    public final static int LOOPS = 1000;

    private EventClickListener mListener;
    private Context mContext;
    private ArrayList<EventModel> mEventList;
    private LayoutInflater mInflater;

    public HomeEventPagerAdapter(Context context, EventClickListener listener) {
        this.mContext = context;
        this.mListener = listener;
        mInflater = LayoutInflater.from(mContext);
    }

    public void setEventList(ArrayList<EventModel> eventModelArrayList){
        this.mEventList = eventModelArrayList;
        notifyDataSetChanged();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        position = position % mEventList.size();

        EventModel eventModel = mEventList.get(position);
        if(eventModel != null){
            ItemHomeEventBinding homeEventBinding = ItemHomeEventBinding.inflate(mInflater, container, false);

            homeEventBinding.setEventModel(eventModel);
            homeEventBinding.setEventClickListener(mListener);

            homeEventBinding.getRoot().setTag("Object" + position);

            container.addView(homeEventBinding.getRoot());
            return homeEventBinding.getRoot();
        }
        return null;
    }

    @Override
    public int getCount() {
        return mEventList != null ? (mEventList.size() * LOOPS) : 0;
    }

    public int getActualCount(){
        return mEventList != null ? mEventList.size() : 0;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public float getPageWidth(int position) {
        return 1f;
    }
}
