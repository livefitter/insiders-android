package com.livefitter.theinsiders.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.livefitter.theinsiders.databinding.ItemUpcomingEventBinding;
import com.livefitter.theinsiders.listener.EventClickListener;
import com.livefitter.theinsiders.model.EventModel;

import jp.wasabeef.picasso.transformations.GrayscaleTransformation;


/**
 * Created by LloydM on 3/7/17
 * for Livefitter
 */

public class UpcomingEventViewHolder extends RecyclerView.ViewHolder {
    private ItemUpcomingEventBinding mBinding;

    public static UpcomingEventViewHolder create(LayoutInflater inflater, ViewGroup parent) {

        ItemUpcomingEventBinding binding = ItemUpcomingEventBinding.inflate(inflater, parent, false);

        return new UpcomingEventViewHolder(binding);
    }

    private UpcomingEventViewHolder(ItemUpcomingEventBinding binding) {
        super(binding.getRoot());
        mBinding = binding;
    }

    public void bindTo(EventModel eventModel, EventClickListener clickListener) {
        mBinding.setEventModel(eventModel);
        mBinding.setClickListener(clickListener);
        mBinding.executePendingBindings();
    }
}
