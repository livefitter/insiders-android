package com.livefitter.theinsiders.service;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.activity.MainActivity;

import java.util.Map;


public class FirebaseNotificationService extends FirebaseMessagingService {

    private final String TAG = this.getClass().getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        String title = getResources().getString(R.string.app_name);
        String body = "";


        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "push notif: " + remoteMessage.getNotification().toString());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData() != null && remoteMessage.getData().size() > 0) {
            Map<String, String> dataPayload = remoteMessage.getData();
            Log.d(TAG, "Message data payload: " + dataPayload);

            if (dataPayload.containsKey("title") && dataPayload.containsKey("body")) {
                title = dataPayload.get("title");
                body = dataPayload.get("body");
            }
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            body = remoteMessage.getNotification().getBody();
            Log.d(TAG, "Message Notification Body: " + body);
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        if (body != null && !body.isEmpty()) {
            sendNotification(title, body);
        }
    }

    @Override
    public void onDeletedMessages() {
        Log.d(TAG, "Messages deleted");
    }

    private void sendNotification(String title, String message) {
        Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        NotificationCompat.Builder notificationBuilder = null;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Log.d(TAG, "Using OREO notification");
            createChannels();
            notificationBuilder = new NotificationCompat.Builder(this, AppConstants.NOTIF_CHANNEL_ID);
        } else {
            Log.d(TAG, "Using older than OREO notification");
            notificationBuilder = new NotificationCompat.Builder(this);
        }

        notificationBuilder.setContentTitle(title)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(resultPendingIntent)
                .setAutoCancel(true);

        if (message != null && !message.isEmpty()) {
            notificationBuilder.setContentText(message);
        }

        Notification notification = notificationBuilder.build();

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(AppConstants.NOTIF_DEFAULT_ID, notification);
        } else {
            NotificationManagerCompat.from(getApplicationContext())
                    .notify(AppConstants.NOTIF_DEFAULT_ID, notification);
        }*/

        NotificationManagerCompat.from(getApplicationContext())
                .notify(AppConstants.NOTIF_DEFAULT_ID, notification);
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void createChannels() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager.getNotificationChannel(AppConstants.NOTIF_CHANNEL_ID) == null) {
            NotificationChannel channel = new NotificationChannel(AppConstants.NOTIF_CHANNEL_ID,
                    AppConstants.NOTIF_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
    }
}
