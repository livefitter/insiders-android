package com.livefitter.theinsiders.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.linkedin.platform.AccessToken;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.AuthListener;
import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.adapter.WelcomePagerAdapter;
import com.livefitter.theinsiders.fragment.ActivationFragment;
import com.livefitter.theinsiders.fragment.LogInFragment;
import com.livefitter.theinsiders.listener.LinkedInSessionListener;
import com.livefitter.theinsiders.utility.SharedPreferencesHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Set;

public class WelcomeActivity extends BaseActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private WelcomePagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    /**
     * LinkedIn Session manager. Can be instatiated from scratch or with an existing auth token
     */
    LISessionManager liSessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        // Clear the currently stored LinkedIn authToken
        SharedPreferencesHelper.clearLiAuthToken(this);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new WelcomePagerAdapter(getSupportFragmentManager());
        mSectionsPagerAdapter.addFragment(new ActivationFragment(), getResources().getString(R.string.title_sign_up));
        mSectionsPagerAdapter.addFragment(new LogInFragment(), getResources().getString(R.string.title_log_in));

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(mViewPager);

        // Safety net to delete user data
        SharedPreferencesHelper.clearUserData(this);
    }

    public void switchViewPagerFragment(@NonNull Fragment oldFragment, @NonNull Fragment newFragment) {
        if (mSectionsPagerAdapter != null) {
            mSectionsPagerAdapter.replaceFragment(oldFragment, newFragment);
        }
    }

    @Override
    public void onBackPressed() {
        // If we're currently on the first tab, navigate back to the ActivationFragment is we're on the SignUpFragment
        if (mViewPager.getCurrentItem() == 0 && AppConstants.FRAG_CURRENT.equals(AppConstants.FRAG_SIGN_UP)) {
            // Check if the currently active fragment is the SignUpFragment
            // If so, replace it with an instance of the ActivationFragment
            mSectionsPagerAdapter.replaceFragment(mSectionsPagerAdapter.getItem(0), new ActivationFragment());
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Checks for any existing LI sessions:
     * <ul>
     * <li>
     * If yes, retrieve that session's auth token for later use.
     * </li>
     * <li>
     * If not, determine if we can initialize a session from an old auth token or initialize it from scratch.
     * </li>
     * </ul>
     * <p>
     * Either way, we make a call to {@link WelcomeActivity#onLinkedInSessionReady()} if it's ready
     * or {@link #onLinkedInSessionError(String message)} if something went wrong.
     */
    public void initializeLinkedInAuthentication() {

        boolean accessTokenValid = LISessionManager.getInstance(this).getSession().isValid();
        if (accessTokenValid) {
            Log.i(getClassTag(), "Reusing existing LinkedIn session");
            liSessionManager = LISessionManager.getInstance(this);

            String liAuthToken = liSessionManager.getSession().getAccessToken().getValue();
            long liAuthExpiry = liSessionManager.getSession().getAccessToken().getExpiresOn();

            if (!liAuthToken.isEmpty()) {
                SharedPreferencesHelper.saveLiAuthToken(WelcomeActivity.this, liAuthToken, liAuthExpiry);
            }

            onLinkedInSessionReady();

        } else {
            // Check if we already have the linkedin auth token stored in sharedPreferences
            AccessToken liAuthToken = SharedPreferencesHelper.getLiAuthToken(this);

            if (!liAuthToken.getValue().isEmpty()) {
                Log.i(getClassTag(), "New LinkedIn session via old AccessToken");
                liSessionManager = LISessionManager.getInstance(this);
                liSessionManager.init(liAuthToken);

                // If successful, do not continue as we already have the linkedin session ready
                if (liSessionManager.getSession().isValid()) {
                    onLinkedInSessionReady();
                }/*else{
                    onLinkedInSessionError();
                }*/
                return;
            }

            // Retrieve a new linkedin authToken from the app
            Log.i(getClassTag(), "New LinkedIn session via new AccessToken");
            liSessionManager = LISessionManager.getInstance(this);
            liSessionManager.init(this, AppConstants.LINKEDIN_SCOPE, new AuthListener() {
                @Override
                public void onAuthSuccess() {
                    // Authentication was successful.
                    // You can now do other calls with the SDK.
                    Log.i(getClassTag(), "LinkedIn AuthSuccess");

                    String liAuthToken = liSessionManager.getSession().getAccessToken().getValue();
                    long liAuthExpiry = liSessionManager.getSession().getAccessToken().getExpiresOn();

                    if (!liAuthToken.isEmpty()) {
                        SharedPreferencesHelper.saveLiAuthToken(WelcomeActivity.this, liAuthToken, liAuthExpiry);
                    }

                    onLinkedInSessionReady();
                }

                @Override
                public void onAuthError(LIAuthError error) {
                    // Handle authentication errors
                    Log.e(getClassTag(), "LIAuthError: " + error.toString());
                    String errorMessage = "";
                    try {
                        JSONObject jsonObject = new JSONObject(error.toString());

                        if (jsonObject.has("errorMessage") && !jsonObject.getString("errorMessage").isEmpty()) {
                            errorMessage = jsonObject.getString("errorMessage");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        onLinkedInSessionError(errorMessage);
                    }
                }
            }, true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(getClassTag(), "onActivityResult");

        if (data != null && data.getExtras() != null) {
            Bundle bundle = data.getExtras();
            Set<String> setKeys = bundle.keySet();
            for (String sKey : setKeys) {
                if (bundle.get(sKey) != null) {
                    Log.d(getClassTag(), String.format("Extra: (%s:%s)", sKey, bundle.get(sKey).toString()));
                }
            }
        }

        LISessionManager.getInstance(this).onActivityResult(this, requestCode, resultCode, data);
    }

    /**
     * Calls the listeners (child fragments of the viewpager) based on the successful state of the LI session
     */
    private void onLinkedInSessionReady() {
        BaseFragment currentFragment = ((BaseFragment) mSectionsPagerAdapter.getItem(mViewPager.getCurrentItem()));

        if (currentFragment != null && currentFragment instanceof LinkedInSessionListener) {
            LinkedInSessionListener listener = (LinkedInSessionListener) currentFragment;

            listener.onLinkedInSessionReady();
        }
    }

    /**
     * Calls the listeners (child fragments of the viewpager) based on the error state of the LI session
     */
    private void onLinkedInSessionError(String message) {

        BaseFragment currentFragment = ((BaseFragment) mSectionsPagerAdapter.getItem(mViewPager.getCurrentItem()));

        if (currentFragment != null && currentFragment instanceof LinkedInSessionListener) {
            LinkedInSessionListener listener = (LinkedInSessionListener) currentFragment;

            listener.onLinkedInSessionError(message);
        }
    }
}
