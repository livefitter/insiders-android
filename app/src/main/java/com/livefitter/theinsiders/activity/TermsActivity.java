package com.livefitter.theinsiders.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.fragment.TermsFragment;

/**
 * Created by LFT-PC-010 on 6/22/2017.
 */

public class TermsActivity extends BaseActivity {

    private TermsFragment mFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        mFragment = (TermsFragment) getSupportFragmentManager().findFragmentById(R.id.terms_fragment_terms);
    }

    @Override
    public void onBackPressed() {
        if(mFragment == null) {
            super.onBackPressed();
        }else if(mFragment.onBackPressed()) {
            super.onBackPressed();
        }
    }
}
