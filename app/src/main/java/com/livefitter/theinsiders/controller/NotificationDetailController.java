package com.livefitter.theinsiders.controller;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.binding.BindingUtil;
import com.livefitter.theinsiders.model.Notification;
import com.livefitter.theinsiders.request.ViewNotificationRequest;
import com.livefitter.theinsiders.utility.DialogUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;

import ejdelrosario.framework.utilities.DialogUtil;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by LFT-PC-010 on 6/28/2017.
 */

public class NotificationDetailController extends BaseController {

    private TextView tvTitle, tvDateTime, tvMessage;

    public NotificationDetailController(BaseActivity activity, BaseFragment fragment) {
        super(activity, fragment);
    }

    @Override
    public void initialize(View rootView, Bundle savedInstanceState) {

        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        getActivity().setSupportActionBar(mToolbar);
        getActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvTitle     = (TextView) rootView.findViewById(R.id.notification_textview_title);
        tvDateTime  = (TextView) rootView.findViewById(R.id.notification_textview_date_time);
        tvMessage   = (TextView) rootView.findViewById(R.id.notification_textview_message);

        retrieveNotification();

    }

    private void retrieveNotification(){

        final ProgressDialog pdLoading = DialogUtility.showIndeterminateProgressDialog(getActivity(),
                getActivity().getString(R.string.title_notifications),
                getActivity().getString(R.string.label_loading), false);

        int notificationId = getActivity().getIntent().getIntExtra(AppConstants.KEY_NOTIFICATION_ID, 0);

        ViewNotificationRequest request = new ViewNotificationRequest(getActivity(), notificationId, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                if(pdLoading != null && pdLoading.isShowing()){

                    pdLoading.dismiss();

                }

                DialogUtil.showAlertDialog(getActivity(),
                        getActivity().getString(R.string.label_error),
                        getActivity().getString(R.string.error_generic_error),
                        new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {

                                getActivity().onBackPressed();

                            }
                        });

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if(pdLoading != null && pdLoading.isShowing()){

                    pdLoading.dismiss();

                }

                if(response.isSuccessful()){

                    String errorMessage = null;

                    try {

                        JSONObject jobj = new JSONObject(response.body().string());

                        boolean statusOK = jobj.getBoolean("status");

                        if(statusOK){

                            Gson gson = new Gson();
                            Type type = new TypeToken<Notification>(){}.getType();
                            final Notification notification = gson.fromJson(jobj.getJSONObject("data").toString(), type);

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    setValues(notification);

                                }
                            });

                        }
                        else{

                            errorMessage = jobj.getString("message");

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        errorMessage = getActivity().getString(R.string.error_generic_error);
                    }


                    if(errorMessage != null){

                        showThreadSafeAlertDialog(getActivity().getString(R.string.label_error),
                                errorMessage,
                                new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {

                                        getActivity().onBackPressed();

                                    }
                                });

                    }

                }
                else {

                    showThreadSafeAlertDialog(getActivity().getString(R.string.label_error),
                            getActivity().getString(R.string.error_generic_error),
                            new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {

                                    getActivity().onBackPressed();

                                }
                            });

                }

            }
        });

        request.execute();

    }

    private void setValues(Notification notification){

        String title = notification.getTitle();
        String dateTime = notification.getDateTime();
        String message = notification.getMessage();

        tvTitle.setText(title);
        tvDateTime.setText(dateTime);
        tvMessage.setText(message);
        BindingUtil.setHtmlText(tvMessage, message);

    }

    @Override
    public void onStop() {

    }
}
