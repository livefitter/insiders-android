package com.livefitter.theinsiders.controller;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.activity.AddCreditCardActivity;
import com.livefitter.theinsiders.adapter.CreditCardListAdapter;
import com.livefitter.theinsiders.fragment.AddCreditCardFragment;
import com.livefitter.theinsiders.fragment.OrderConfirmedFragment;
import com.livefitter.theinsiders.listener.CreditCardClickListener;
import com.livefitter.theinsiders.model.CreditCard;
import com.livefitter.theinsiders.model.EventModel;
import com.livefitter.theinsiders.model.Guest;
import com.livefitter.theinsiders.model.PromoCode;
import com.livefitter.theinsiders.request.RemoveCardRequest;
import com.livefitter.theinsiders.request.RsvpGuestsRequest;
import com.livefitter.theinsiders.request.SaveCardRequest;
import com.livefitter.theinsiders.request.UserCreditCardListRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import ejdelrosario.framework.utilities.DialogUtil;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by LFT-PC-010 on 7/7/2017.
 */

public class CreditCardListController extends BaseController implements CreditCardClickListener {

    private RecyclerView rvCreditCardList;
    private Button btnProceed, btnRemoveCard;
    private boolean isCheckOutMode;

    private ProgressDialog pdLoading;

    private ArrayList<CreditCard> creditCards = new ArrayList<>();

    public CreditCardListController(BaseActivity activity, BaseFragment fragment) {
        super(activity, fragment);
    }

    @Override
    public void initialize(View rootView, Bundle savedInstanceState) {

        if(getFragment().getArguments() != null) {

            isCheckOutMode = getFragment().getArguments().getBoolean(AppConstants.KEY_CHECK_OUT_MODE, false);

        }
        else{

            isCheckOutMode = false;

        }

        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        getActivity().setSupportActionBar(mToolbar);
        getActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getActivity().getSupportActionBar().setTitle(isCheckOutMode ? getActivity().getString(R.string.label_check_out) : getActivity().getString(R.string.label_payment_details));

        rvCreditCardList = (RecyclerView) rootView.findViewById(R.id.check_out_recyclerview_cards);
        btnProceed = (Button) rootView.findViewById(R.id.check_out_button_proceed);
        btnRemoveCard = (Button) rootView.findViewById(R.id.check_out_button_remove_card);

        rvCreditCardList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvCreditCardList.setHasFixedSize(false);

        // Hide the cards until we can be sure that we have cards in the list
        //btnRemoveCard.setVisibility(isCheckOutMode ? View.GONE : View.VISIBLE);
        btnRemoveCard.setVisibility(View.GONE);
        btnProceed.setVisibility(View.GONE);
        btnProceed.setText(isCheckOutMode ? getActivity().getString(R.string.label_proceed) : getActivity().getString(R.string.label_set_default));

        btnRemoveCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String cardToken = null;

                for(CreditCard card : creditCards){

                    if(card.isSelected()){

                        cardToken = card.getStripeCardId();
                        break;

                    }

                }

                if(cardToken != null) {

                    final String finalCardToken = cardToken;
                    DialogUtil.showConfirmationDialog(getActivity(),
                            getActivity().getString(R.string.msg_confirm_remove_card),
                            getActivity().getString(R.string.label_yes),
                            getActivity().getString(R.string.label_no),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    if(which == DialogInterface.BUTTON_POSITIVE) {
                                        removeCard(finalCardToken);
                                    }

                                }
                            });

                }

            }
        });

        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isCheckOutMode){

                    doPurchase();

                }
                else{

                    String cardToken = null;

                    for(CreditCard card : creditCards){

                        if(card.isSelected()){

                            cardToken = card.getStripeCardId();
                            break;

                        }

                    }

                    if(cardToken != null) {

                        saveDefault(cardToken);

                    }

                }

            }
        });

        rootView.findViewById(R.id.check_out_button_add_payment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //isCheckOutMode == true means this fragment was called directly and not from CreditCardListActivity
                if(isCheckOutMode){

                    AddCreditCardFragment addCreditCardFragment = new AddCreditCardFragment();

                    addCreditCardFragment.setArguments(getFragment().getArguments());

                    getActivity().switchFragment(
                            addCreditCardFragment,
                            R.id.checkout_fragment_container,
                            AppConstants.FRAG_ADD_CREDIT_CARD,
                            true,
                            false);

                }
                else {

                    Intent intent = new Intent(getActivity(), AddCreditCardActivity.class);
                    getActivity().switchActivity(intent, false);

                }

            }
        });

    }

    private void showProgressDialog(String message){

        if(pdLoading == null){

            pdLoading = new ProgressDialog(getActivity());
            pdLoading.setCancelable(false);

        }

        pdLoading.setMessage(message);

        if(!pdLoading.isShowing()) {
            pdLoading.show();
        }

    }

    private void hideProgressDialog(){

        if(pdLoading != null && pdLoading.isShowing()){

            pdLoading.dismiss();

        }

    }

    public void getCardList(){

        showProgressDialog(getActivity().getString(R.string.label_loading));

        UserCreditCardListRequest request = new UserCreditCardListRequest(getActivity(), new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                hideProgressDialog();

                showThreadSafeAlertDialog(getActivity().getString(R.string.label_error),
                        getActivity().getString(R.string.error_generic_error));

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                hideProgressDialog();

                String errorMessage = null;

                if(response.isSuccessful()){

                    try {

                        JSONObject jobj = new JSONObject(response.body().string());

                        boolean statusOk = jobj.getBoolean("status");

                        if(statusOk){

                            Gson gson = new Gson();
                            Type type = new TypeToken<ArrayList<CreditCard>>(){}.getType();

                            creditCards = gson.fromJson(jobj.getJSONArray("data").toString(), type);

                            populateCreditCardList();

                        }
                        else{

                            errorMessage = jobj.getString("message");

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        errorMessage = getActivity().getString(R.string.error_generic_error);
                    }

                }
                else{

                    errorMessage = getActivity().getString(R.string.error_generic_error);

                }

                if(errorMessage != null){

                    showThreadSafeAlertDialog(getActivity().getString(R.string.label_error), errorMessage);

                }

            }
        });

        request.execute();

    }

    private void populateCreditCardList() {

        for(CreditCard card :  creditCards){

            if(card.isIsDefault()){

                card.setSelected(true);

            }
            else{

                card.setSelected(false);

            }

        }

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                CreditCardListAdapter adapter = new CreditCardListAdapter(getActivity(), creditCards, CreditCardListController.this);
                rvCreditCardList.setAdapter(adapter);

                if (creditCards != null && !creditCards.isEmpty()) {
                    btnRemoveCard.setVisibility(isCheckOutMode ? View.GONE : View.VISIBLE);
                    btnProceed.setVisibility(View.VISIBLE);
                }else{
                    btnRemoveCard.setVisibility(View.GONE);
                    btnProceed.setVisibility(View.GONE);
                }
            }
        });

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onCardClick(CreditCard selectedCard) {

        for(CreditCard card : creditCards){

            if(card.getStripeCardId().equals(selectedCard.getStripeCardId())){

                card.setSelected(true);

            }
            else{

                card.setSelected(false);

            }

        }

    }

    private void doPurchase(){

        EventModel mEventModel = getFragment().getArguments().getParcelable(AppConstants.KEY_EVENT_MODEL);
        ArrayList<Guest> invitedGuests = getFragment().getArguments().getParcelableArrayList(AppConstants.KEY_INVITED_GUEST_LIST);
        PromoCode promoCode = getFragment().getArguments().getParcelable(AppConstants.KEY_PROMO_CODE);

        int quantity = invitedGuests.size() + (mEventModel.getHasRsvped() ? 0 : 1);

        String cardToken = null;

        for(CreditCard card : creditCards){

            if(card.isSelected()){

                cardToken = card.getStripeCardId();
                break;

            }

        }

        if(cardToken == null){

            DialogUtil.showAlertDialog(getActivity(), getActivity().getString(R.string.msg_add_payment_method));
            return;

        }

        showProgressDialog(getActivity().getString(R.string.label_purchasing_tickets));

        RsvpGuestsRequest request = new RsvpGuestsRequest(getActivity(), mEventModel.getId(), invitedGuests, !mEventModel.getHasRsvped(), quantity, cardToken, promoCode, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                hideProgressDialog();

                showThreadSafeAlertDialog(getActivity().getString(R.string.label_error),
                        getActivity().getString(R.string.error_generic_error));

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                hideProgressDialog();

                String errorMessage = null;

                if(response.isSuccessful()) {

                    try {

                        JSONObject jobj = new JSONObject(response.body().string());

                        boolean statusOk = jobj.getBoolean("status");

                        if(!statusOk){

                            errorMessage = jobj.getString("message");

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        errorMessage = getActivity().getString(R.string.error_generic_error);
                    }

                }
                else{

                    errorMessage = getActivity().getString(R.string.error_generic_error);

                }

                if(errorMessage != null){

                    showThreadSafeAlertDialog(getActivity().getString(R.string.label_error), errorMessage);

                }
                else{

                    showThreadSafeAlertDialog(getActivity().getString(R.string.label_check_out),
                            getActivity().getString(R.string.msg_purchase_successful), new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {

                                    OrderConfirmedFragment orderConfirmedFragment = new OrderConfirmedFragment();

                                    orderConfirmedFragment.setArguments(getFragment().getArguments());

                                    getActivity().switchFragment(orderConfirmedFragment,
                                            R.id.checkout_fragment_container,
                                            AppConstants.FRAG_ORDER_CONFIRMED,
                                            true,
                                            false);

                                }
                            });

                }

            }
        });

        request.execute();

    }

    private void saveDefault(String cardTokenId){

        showProgressDialog(getActivity().getString(R.string.label_loading));

        try {

            JSONObject jobjParams = createJsonParams(cardTokenId);

            SaveCardRequest request = new SaveCardRequest(getActivity(), jobjParams, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                    hideProgressDialog();

                    showThreadSafeAlertDialog(getActivity().getString(R.string.label_error),
                            getActivity().getString(R.string.error_generic_error));

                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {

                    hideProgressDialog();

                    String errorMessage = null;

                    if(response.isSuccessful()) {

                        try {

                            JSONObject jobj = new JSONObject(response.body().string());

                            boolean statusOK = jobj.getBoolean("status");

                            if(!statusOK){

                                errorMessage = jobj.getString("message");

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            errorMessage = getActivity().getString(R.string.error_generic_error);
                        }

                    }
                    else{

                        errorMessage = getActivity().getString(R.string.error_generic_error);

                    }

                    if(errorMessage != null){

                        showThreadSafeAlertDialog(getActivity().getString(R.string.label_error), errorMessage);

                    }
                    else{

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                DialogUtil.showToast(getActivity(), getActivity().getString(R.string.msg_card_save_default_success), Toast.LENGTH_LONG);

                            }
                        });

                    }

                }
            });

            request.execute();

        } catch (JSONException e) {
            e.printStackTrace();

            hideProgressDialog();
            DialogUtil.showAlertDialog(getActivity(), getActivity().getString(R.string.error_generic_error));
        }

    }

    private JSONObject createJsonParams(String cardTokenId) throws JSONException {

        JSONObject jobj = new JSONObject();
        JSONObject jobjData = new JSONObject();

        jobjData.put("is_default", true);
        jobjData.put("stripe_token", cardTokenId);

        jobj.put("data", jobjData);

        return jobj;
    }

    private void removeCard(String cardTokenId){

        showProgressDialog(getActivity().getString(R.string.label_removing_card));

        try {

            JSONObject jobjParams = createJsonParams(cardTokenId);

            RemoveCardRequest request = new RemoveCardRequest(getActivity(), jobjParams, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                    hideProgressDialog();

                    showThreadSafeAlertDialog(getActivity().getString(R.string.label_error),
                            getActivity().getString(R.string.error_generic_error));

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    hideProgressDialog();

                    String errorMessage = null;

                    if(response.isSuccessful()) {

                        try {

                            JSONObject jobj = new JSONObject(response.body().string());

                            boolean statusOK = jobj.getBoolean("status");

                            if(!statusOK){

                                errorMessage = jobj.getString("message");

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            errorMessage = getActivity().getString(R.string.error_generic_error);
                        }

                    }
                    else{

                        errorMessage = getActivity().getString(R.string.error_generic_error);

                    }

                    if(errorMessage != null){

                        showThreadSafeAlertDialog(getActivity().getString(R.string.label_error), errorMessage);

                    }
                    else{

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                DialogUtil.showToast(getActivity(), getActivity().getString(R.string.msg_card_remove_success), Toast.LENGTH_LONG);

                                getCardList();

                            }
                        });

                    }

                }

            });


            request.execute();

        } catch (JSONException e) {
            e.printStackTrace();

            hideProgressDialog();
            DialogUtil.showAlertDialog(getActivity(), getActivity().getString(R.string.error_generic_error));
        }

    }

}
