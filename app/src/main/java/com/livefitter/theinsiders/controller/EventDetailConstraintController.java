package com.livefitter.theinsiders.controller;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.activity.RsvpActivity;
import com.livefitter.theinsiders.databinding.FragmentEventDetailConstraintBinding;
import com.livefitter.theinsiders.model.EventModel;
import com.livefitter.theinsiders.model.Guest;
import com.livefitter.theinsiders.request.RetrieveEventDetailRequest;
import com.livefitter.theinsiders.request.RsvpGuestsRequest;
import com.livefitter.theinsiders.utility.DialogUtility;
import com.livefitter.theinsiders.view.GuestConfirmationDialogFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import ejdelrosario.framework.utilities.DialogUtil;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by LloydM on 3/1/17
 * for Livefitter
 */

public class EventDetailConstraintController extends BaseController implements View.OnClickListener {

    private FragmentEventDetailConstraintBinding mBinding;

    private FrameLayout flBlocker;
    private RelativeLayout rlInviteStepperLayout;
    private ImageButton btnMinusInvite, btnPlusInvite;
    private Button btnRsvp, btnViewInvites;
    private TextView tvGuestCount, tvGuestMax;
    private LinearLayout llGuestListLayout;
    private ProgressDialog dProgress;

    private EventModel mEvent;

    /**
     * Total number of guests invited before
     */
    private int mPrevGuestCount = 0;
    /**
     * Current guest count for this invitation session.
     * Does not take into account the number of guests from previous invitation session
     */
    private int mGuestCount = 0;
    /**
     * Allowable number of guests for this session.
     * Based on a algorithm detailed in {@link #onEventDetailsReceived}
     */
    private int mMaxGuestCount = 0;

    /**
     * Flag that is set when we are inviting guests.
     * Used to determine if whe should show the RSVP screen right after invitation processing finishes
     */
    private boolean isInvitingGuests = false;

    /**
     * Flag that is set when we need to retrieve the entire Event details
     */
    private boolean shouldRetrieveEventDetails = true;

    /**
     * Flag that is set if this event is expired.
     * This is only set when we're coming from the PastEvents screen
     */
    private boolean isEventExpired;

    public EventDetailConstraintController(BaseActivity activity, BaseFragment fragment) {
        super(activity, fragment);

        Bundle extras = getActivity().getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(AppConstants.KEY_EVENT_MODEL)) {
                mEvent = extras.getParcelable(AppConstants.KEY_EVENT_MODEL);

                isEventExpired = extras.getBoolean(AppConstants.KEY_EVENT_EXPIRED, false);
            }
        }

    }

    public void retrieveFullEventDetails() {

        if (!shouldRetrieveEventDetails) {
            return;
        }

        dProgress = DialogUtility.showIndeterminateProgressDialog(getActivity(),
                "",
                getFragment().getString(R.string.label_retrieving_event_detail),
                false);

        RetrieveEventDetailRequest request = new RetrieveEventDetailRequest(getActivity(), mEvent.getId(), new Callback() {

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (dProgress != null && dProgress.isShowing()) {
                    dProgress.dismiss();
                }

                if (response.isSuccessful()) {
                    String responseBody = response.body().string();
                    if (response.isSuccessful() && !responseBody.isEmpty() && !responseBody.equals("{}")) {
                        Log.d(getFragment().getClassTag(), "onResponse body: " + responseBody);

                        try {
                            JSONObject responseBodyJson = new JSONObject(responseBody);

                            if (responseBodyJson.has("status") && responseBodyJson.has("data")) {
                                boolean statusOK = responseBodyJson.getBoolean("status");

                                if (statusOK) {

                                    shouldRetrieveEventDetails = false;

                                    String dataResponse = responseBodyJson.getString("data");

                                    Gson gson = new Gson();
                                    Type typeToken = new TypeToken<EventModel>() {
                                    }.getType();
                                    EventModel eventModel = gson.fromJson(dataResponse, typeToken);

                                    if (eventModel != null) {
                                        mEvent.setAllFields(eventModel);
                                        onEventDetailsReceived();
                                    }
                                }
                            } else {

                                String errorMessage = responseBodyJson.getString("message");

                                showThreadSafeAlertDialog(
                                        getActivity().getString(R.string.label_error),
                                        errorMessage,
                                        new DialogInterface.OnDismissListener() {
                                            @Override
                                            public void onDismiss(DialogInterface dialogInterface) {
                                                getActivity().finish();
                                            }
                                        });

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            showThreadSafeAlertDialog(
                                    getActivity().getString(R.string.label_error),
                                    getActivity().getString(R.string.error_generic_error),
                                    new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialogInterface) {
                                            getActivity().finish();
                                        }
                                    });
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(getFragment().getClassTag(), "onFailure exception " + e.getMessage());

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (dProgress != null && dProgress.isShowing()) {
                            dProgress.dismiss();
                        }

                        DialogUtil.showAlertDialog(getActivity(),
                                getActivity().getString(R.string.label_error),
                                getActivity().getString(R.string.error_generic_error),
                                new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialogInterface) {
                                        getActivity().finish();
                                    }
                                });

                    }
                });
            }
        });

        request.execute();
    }

    /**
     * Clean up of screen elements based on the contents of the EventModel details
     */
    private void onEventDetailsReceived() {

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Reset mGuestCount and the counter
                mGuestCount = 0;
                tvGuestCount.setText(String.valueOf(mGuestCount));

                // Clear all child views from llGuestListLayout, if any
                if (llGuestListLayout.getChildCount() > 0) {
                    llGuestListLayout.removeAllViews();
                }

                // Slot taken if user has reserved or not
                int userSlot = mEvent.getHasRsvped() ? 0 : 1;

                // Determine if rlInviteStepperLayout should be visible or not
                /*int inviteStepperLayoutVisibility = View.GONE;*/

                /*if(mEvent.getHasRsvped()){
                    if(mEvent.getRemainingSlots() > 0){
                        inviteStepperLayoutVisibility = View.VISIBLE;
                    }
                }else{
                    if(mEvent.getRemainingSlots() > 1 && mEvent.getRemainingAdditionalGuests() > 0){
                        inviteStepperLayoutVisibility = View.VISIBLE;
                    }
                }*/

                /*if(mEvent.getRemainingSlots() == 0 || mEvent.getRemainingAdditionalGuests() == 0){
                    inviteStepperLayoutVisibility = View.GONE;
                }
                else if(mEvent.getRemainingAdditionalGuests() <= (mEvent.getRemainingSlots() - userSlot) && !isEventExpired){
                    inviteStepperLayoutVisibility = View.VISIBLE;
                }*/

                // Determine how many guests are actually allowed for the user to invite
                // based on whether they have rsvp prior or not as booking slots prioritizes the user before their guests

//                if(mEvent.getRemainingAdditionalGuests() == 0 || mEvent.getRemainingSlots() == 0){
//                    mMaxGuestCount = 0;
//                }
                if (mEvent.getRemainingSlots() == 0 || (mEvent.getRemainingAdditionalGuests() + userSlot) == 0 || isEventExpired) {
                    mMaxGuestCount = 0;
                } else if (mEvent.getRemainingAdditionalGuests() >= (mEvent.getRemainingSlots() - userSlot)) {
                    // Subtract one to account for the user taking up a slot
                    mMaxGuestCount = mEvent.getRemainingSlots() - userSlot;
                } else {
                    mMaxGuestCount = mEvent.getRemainingAdditionalGuests();
                }

                // Determine if btnPurchase should be visible or not
                int rsvpButtonVisibility = View.GONE;

                if (mEvent.getHasRsvped()) {
                    if (mMaxGuestCount > 0 && !isEventExpired) {
                        rsvpButtonVisibility = View.VISIBLE;
                    }
                } else {
                    if (mEvent.getRemainingSlots() > 0 && !isEventExpired) {
                        rsvpButtonVisibility = View.VISIBLE;
                    }
                }

                // Determine if btnViewInvites should be visible or not
                int viewInvitationButtonVisibility = mEvent.hasInvitedBasedOnGuestsDifference() ? View.VISIBLE : View.GONE;

                mPrevGuestCount = mEvent.getTotalInvitedGuests();

//                tvGuestCount.setText(String.valueOf(mPrevGuestCount));
                tvGuestMax.setText(String.valueOf(mMaxGuestCount));
                rlInviteStepperLayout.setVisibility(mMaxGuestCount > 0 ? View.VISIBLE : View.GONE);

                btnRsvp.setVisibility(rsvpButtonVisibility);
                btnViewInvites.setVisibility(viewInvitationButtonVisibility);

                flBlocker.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void initialize(View rootView, Bundle savedInstanceState) {

        mToolbar = rootView.findViewById(R.id.toolbar);
        getActivity().setSupportActionBar(mToolbar);
        getActivity().enableUpbutton();
        getFragment().setHasOptionsMenu(true);

        flBlocker = rootView.findViewById(R.id.loading_blocker);
        rlInviteStepperLayout = rootView.findViewById(R.id.event_detail_invite_stepper_layout);
        tvGuestCount = rootView.findViewById(R.id.event_detail_invite_stepper_count);
        tvGuestMax = rootView.findViewById(R.id.event_detail_invite_stepper_max);
        btnMinusInvite = rootView.findViewById(R.id.event_detail_invite_stepper_minus);
        btnPlusInvite = rootView.findViewById(R.id.event_detail_invite_stepper_plus);
        btnRsvp = rootView.findViewById(R.id.event_detail_invite_rsvp_button);
        btnViewInvites = rootView.findViewById(R.id.event_detail_invite_view_invitation_button);
        llGuestListLayout = rootView.findViewById(R.id.event_detail_invite_guest_layout);

        btnMinusInvite.setOnClickListener(this);
        btnPlusInvite.setOnClickListener(this);
        btnRsvp.setOnClickListener(this);
        btnViewInvites.setOnClickListener(this);

        tvGuestCount.setText(String.valueOf(0));

        if (mEvent != null) {
            mBinding.setEventModel(mEvent);
            mBinding.setIsEventExpired(isEventExpired);
        }
    }

    public boolean onBackPressed() {
        return true;
    }

    @Override
    public void onClick(View view) {

        Log.d(getFragment().getClassTag(), "mPrevGuestCount: " + mPrevGuestCount + ", mGuestCount: " + mGuestCount);

        if (view == btnMinusInvite
                && mGuestCount > 0) {
            removeGuestField();
        } else if (view == btnPlusInvite
                && mGuestCount < mMaxGuestCount) {
            addGuestField();
        } else if (view == btnRsvp) {
            checkGuestList();
        } else if (view == btnViewInvites) {
            goToRsvpScreen();
        }
    }

    private void removeGuestField() {
        if (llGuestListLayout != null) {
            String tag = "Invitee:" + mGuestCount;
            View inviteeViewTobeRemoved = llGuestListLayout.findViewWithTag(tag);

            if (inviteeViewTobeRemoved != null) {

                llGuestListLayout.removeView(inviteeViewTobeRemoved);
                mGuestCount--;

                tvGuestCount.setText(String.valueOf(mGuestCount));
            }
        }
    }

    private void addGuestField() {
        if (llGuestListLayout != null) {
            mGuestCount++;
            String tag = "Invitee:" + mGuestCount;
            View inviteeView = LayoutInflater.from(getActivity()).inflate(R.layout.event_detail_guest, llGuestListLayout, false);
            inviteeView.setTag(tag);

            Spinner sSalutations = (Spinner) inviteeView.findViewById(R.id.event_detail_invite_salutation);
            EditText etInviteeName = (EditText) inviteeView.findViewById(R.id.event_detail_invite_name);

            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                    R.array.array_salutations, android.R.layout.simple_spinner_item);
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sSalutations.setAdapter(adapter);

            etInviteeName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                    boolean handled = false;
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        checkGuestList();
                        handled = true;
                    }
                    return handled;
                }
            });

            llGuestListLayout.addView(inviteeView);

            tvGuestCount.setText(String.valueOf(mGuestCount));
        }
    }

    private void checkGuestList() {

        ArrayList<Guest> guestList = retrieveGuestList();

        // Check if the guestList is empty if our guestCount is not zero
        if (mEvent.getHasRsvped() && mGuestCount == 0) {

            DialogUtil.showAlertDialog(getActivity(),
                    getFragment().getString(R.string.label_error),
                    getFragment().getString(R.string.error_invite_guest));

        }else if ((mGuestCount > 0 && guestList.isEmpty())
                    || (mGuestCount > 0 && mGuestCount != guestList.size())) {

            DialogUtil.showAlertDialog(getActivity(),
                    getFragment().getString(R.string.label_error),
                    getFragment().getString(R.string.error_no_guests_selected));

        } else {
            //Otherwise, continue on even if the list is empty
            // As we can still RSVP the user
            showGuestConfirmationDialog(guestList);
        }

        /*if((mGuestCount == 0 && !guestList.isEmpty())||
                (mGuestCount > 0 && !guestList.isEmpty())){
            showGuestConfirmationDialog(guestList);
        }else{
            DialogUtil.showAlertDialog(getActivity(),
                    getFragment().getString(R.string.label_error),
                    getFragment().getString(R.string.error_no_guests_selected));
        }*/
    }

    /**
     * Traverses the children in llGuestListLayout and reads the salutations
     * and names included within and uses them to create Guest instances.
     * This only happens when the name for a child is present.
     *
     * @return List of guests
     */
    private ArrayList<Guest> retrieveGuestList() {
        int childCount = llGuestListLayout.getChildCount();

        ArrayList<Guest> guestList = new ArrayList<Guest>();

        for (int i = 0; i < childCount; i++) {
            View inviteeView = llGuestListLayout.getChildAt(i);
            if (inviteeView != null) {
                Spinner sSalutations = (Spinner) inviteeView.findViewById(R.id.event_detail_invite_salutation);
                EditText etInviteeName = (EditText) inviteeView.findViewById(R.id.event_detail_invite_name);

                if (sSalutations != null && etInviteeName != null) {
                    String salutation = sSalutations.getSelectedItem().toString();
                    String guestName = "";

                    // Ensure that the name has no trailing whitespaces
                    String[] splitGuestName = etInviteeName.getText().toString().split(" ");
                    for (int j = 0; j < splitGuestName.length; j++) {

                        String value = splitGuestName[j];

                        if (!value.isEmpty() || value.matches("([\\w])+")) {
                            guestName = guestName.concat(value);

                            if (j < splitGuestName.length - 1) {
                                guestName = guestName.concat(" ");
                            }
                        }
                    }


                    if (!guestName.isEmpty()) {
                        Guest guest = new Guest(salutation, guestName);
                        guestList.add(guest);
                    }
                }
            }
        }

        isInvitingGuests = !guestList.isEmpty();

        return guestList;
    }

    /**
     * Uses {@link GuestConfirmationDialogFragment} to display the list of valid guests.
     * Upon the user tapping "Yes", sends this information to the RSVP activity.
     *
     * @param guestList list of valid guests with a complete salutation and name.
     */
    private void showGuestConfirmationDialog(final ArrayList<Guest> guestList) {
        GuestConfirmationDialogFragment dGuestConfirmation = new GuestConfirmationDialogFragment();
        dGuestConfirmation.setRsvpUser(!mEvent.getHasRsvped());
        dGuestConfirmation.setGuestList(guestList);
        dGuestConfirmation.setClickListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                if (which == Dialog.BUTTON_POSITIVE) {
                    rsvpGuests(guestList);
                }
            }
        });

        dGuestConfirmation.show(getActivity().getSupportFragmentManager(), "Guest Confirmation");
    }

    private void rsvpGuests(ArrayList<Guest> guestList) {
        int dialogMessageResId = 0;

        if (!mEvent.getHasRsvped() && guestList.isEmpty()) {
            dialogMessageResId = R.string.label_reserving_event;
        } else if (!mEvent.getHasRsvped() && !guestList.isEmpty()) {
            dialogMessageResId = R.string.label_reserving_event_with_guests;
        } else {
            dialogMessageResId = R.string.label_reserving_event_for_guests;
        }

        String dialogMessage = getFragment().getString(dialogMessageResId);

        dProgress = DialogUtility.showIndeterminateProgressDialog(getActivity(),
                getFragment().getString(R.string.title_rsvp),
                dialogMessage,
                false);

        RsvpGuestsRequest request = new RsvpGuestsRequest(getActivity(), mEvent.getId(), guestList, !mEvent.getHasRsvped(), new Callback() {

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (dProgress != null && dProgress.isShowing()) {
                    dProgress.dismiss();
                }


                if (response.isSuccessful()) {
                    String responseBody = response.body().string();
                    if (response.isSuccessful() && !responseBody.isEmpty() && !responseBody.equals("{}")) {
                        Log.d(getFragment().getClassTag(), "onResponse body: " + responseBody);

                        try {
                            final JSONObject responseBodyJson = new JSONObject(responseBody);

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    handleRsvpResponse(responseBodyJson);
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                            showThreadSafeAlertDialog(
                                    getActivity().getString(R.string.label_error),
                                    getActivity().getString(R.string.error_generic_error));
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(getFragment().getClassTag(), "onFailure exception " + e.getMessage());

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (dProgress != null && dProgress.isShowing()) {
                            dProgress.dismiss();
                        }

                        DialogUtil.showAlertDialog(getActivity(),
                                getActivity().getString(R.string.label_error),
                                getActivity().getString(R.string.error_generic_error));

                    }
                });
            }
        });

        request.execute();
    }

    private void handleRsvpResponse(JSONObject responseBodyJson) {
        try {

            if (mEvent.getHasRsvped()) {
                boolean rsvpStatusOK = responseBodyJson.getBoolean("status");
                String responseDialogMessage = responseBodyJson.getString("message");

                if (rsvpStatusOK) {
                    DialogUtil.showAlertDialog(getActivity(),
                            getFragment().getString(R.string.label_rsvp),
                            responseDialogMessage,
                            new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    goToRsvpScreen();
                                }
                            });
                } else {
                    DialogUtil.showAlertDialog(getActivity(),
                            getFragment().getString(R.string.label_error),
                            responseDialogMessage);
                }
            } else {
                // Check if the status messages exist
                boolean rsvpStatusOK = responseBodyJson.has("status") && responseBodyJson.getBoolean("status");
                boolean guestStatusOK = responseBodyJson.has("status_guests") && responseBodyJson.getBoolean("status_guests");

                String messageGuests = responseBodyJson.getString("message_guests");
                String messageRsvp = responseBodyJson.getString("message_rsvp");

                //if (!messageRsvp.isEmpty() && !messageGuests.isEmpty()) {
                if (isInvitingGuests) {
                    // We're RSVP-ing the user and some guests
                    Spannable spanRsvp = new SpannableString(messageRsvp);
                    if (!rsvpStatusOK) {
                        spanRsvp.setSpan(new ForegroundColorSpan(Color.RED), 0, messageRsvp.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                    }

                    Spannable spanGuests = new SpannableString(messageGuests);
                    if (!guestStatusOK) {
                        spanGuests.setSpan(new ForegroundColorSpan(Color.RED), 0, messageGuests.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                    }

                    String responseDialogMessage = spanRsvp + "\n\n" + spanGuests;

                    if (rsvpStatusOK) {
                        //retrieve the eventDetails at a later time
                        shouldRetrieveEventDetails = true;
                        if (guestStatusOK) {
                            DialogUtil.showAlertDialog(getActivity(),
                                    getFragment().getString(R.string.label_rsvp),
                                    responseDialogMessage,
                                    new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialogInterface) {
                                            goToRsvpScreen();
                                        }
                                    });
                        } else {
                            DialogUtil.showAlertDialog(getActivity(),
                                    getFragment().getString(R.string.label_rsvp),
                                    responseDialogMessage,
                                    new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialogInterface) {
                                            // refresh the screen right now to reflect the changes
                                            retrieveFullEventDetails();
                                        }
                                    });
                        }
                    } else {
                        DialogUtil.showAlertDialog(getActivity(),
                                getFragment().getString(R.string.label_error),
                                responseDialogMessage);
                    }
                    //} else if (!messageRsvp.isEmpty() && messageGuests.isEmpty()) {
                } else {
                    // We're only rsvp-ing for the user
                    Spannable spanRsvp = new SpannableString(messageRsvp);
                    if (!rsvpStatusOK) {
                        spanRsvp.setSpan(new ForegroundColorSpan(Color.RED), 0, messageRsvp.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                    }

                    String responseDialogMessage = spanRsvp.toString();

                    if (rsvpStatusOK) {
                        //retrieve the eventDetails right now
                        shouldRetrieveEventDetails = true;
                        DialogUtil.showAlertDialog(getActivity(),
                                getFragment().getString(R.string.label_rsvp),
                                responseDialogMessage,
                                new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialogInterface) {
                                        // refresh the screen right now to reflect the changes
                                        retrieveFullEventDetails();
                                    }
                                });

                    } else {
                        DialogUtil.showAlertDialog(getActivity(),
                                getFragment().getString(R.string.label_error),
                                responseDialogMessage);
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
            DialogUtil.showAlertDialog(getActivity(),
                    getActivity().getString(R.string.label_error),
                    getActivity().getString(R.string.error_generic_error));
        }
    }

    private void goToRsvpScreen() {
        Intent intent = new Intent(getActivity(), RsvpActivity.class);
        intent.putExtra(AppConstants.KEY_EVENT_MODEL, mEvent);
        getActivity().switchActivity(intent, false);
    }

    public void setBinding(FragmentEventDetailConstraintBinding mBinding) {
        this.mBinding = mBinding;
    }

    @Override
    public void onStop() {
        if (dProgress != null && dProgress.isShowing()) {
            dProgress.dismiss();
            dProgress = null;
        }
    }
}
