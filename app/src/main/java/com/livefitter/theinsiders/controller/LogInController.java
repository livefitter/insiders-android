package com.livefitter.theinsiders.controller;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.InputFilter;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.activity.ForgotPasswordActivity;
import com.livefitter.theinsiders.activity.MainActivity;
import com.livefitter.theinsiders.activity.WelcomeActivity;
import com.livefitter.theinsiders.model.UserProfile;
import com.livefitter.theinsiders.request.LinkedInLogInRequest;
import com.livefitter.theinsiders.request.LoginRequest;
import com.livefitter.theinsiders.utility.DialogUtility;
import com.livefitter.theinsiders.utility.EmojiExcludeFilter;
import com.livefitter.theinsiders.utility.FaqTermsUtil;
import com.livefitter.theinsiders.utility.FormFieldTextWatcher;
import com.livefitter.theinsiders.utility.SharedPreferencesHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;

import ejdelrosario.framework.utilities.DialogUtil;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by LloydM on 2/9/17
 * for Livefitter
 */

public class LogInController extends BaseController implements View.OnClickListener, Callback {

    TextInputLayout tilEmail, tilPassword;
    TextInputEditText etEmail, etPassword;
    Button btnLogIn;
    FrameLayout btnLoginLinkedIn;
    TextView tvFaqTerms;

    ProgressDialog dProgress;

    public LogInController(BaseActivity activity, BaseFragment fragment) {
        super(activity, fragment);
    }

    @Override
    public void initialize(View rootView, Bundle savedInstanceState) {
        tilEmail = (TextInputLayout) rootView.findViewById(R.id.edittext_layout_email);
        tilPassword = (TextInputLayout) rootView.findViewById(R.id.edittext_layout_password);

        InputFilter[] inputFilters = new InputFilter[]{new EmojiExcludeFilter()};

        etEmail = (TextInputEditText) rootView.findViewById(R.id.edittext_email);
        etEmail.addTextChangedListener(new FormFieldTextWatcher(tilEmail));
        etEmail.setFilters(inputFilters);

        etPassword = (TextInputEditText) rootView.findViewById(R.id.edittext_password);
        etPassword.addTextChangedListener(new FormFieldTextWatcher(tilPassword));
        etPassword.setFilters(inputFilters);
        etPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    if (areFieldsValid()) {
                        logInUser();
                    }
                    return true;
                }
                return false;
            }
        });

        btnLogIn = (Button) rootView.findViewById(R.id.button_log_in);
        btnLogIn.setOnClickListener(this);

        btnLoginLinkedIn = (FrameLayout) rootView.findViewById(R.id.button_log_in_linkedin);
        btnLoginLinkedIn.setOnClickListener(this);

        tvFaqTerms = (TextView) rootView.findViewById(R.id.textview_faq_terms);
        FaqTermsUtil.setSpannableContent(getActivity(), tvFaqTerms);

        rootView.findViewById(R.id.textview_forgot_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().switchActivity(ForgotPasswordActivity.class, false);
            }
        });
    }

    @Override
    public void onClick(View view) {

        if (view == btnLogIn) {
            if (areFieldsValid()) {
                logInUser();
            }
        } else if (view == btnLoginLinkedIn) {
            //have an own checker for linkedIn app since built it checker brings issues on some devices
            Intent linkedIn = getActivity().getPackageManager().getLaunchIntentForPackage("com.linkedin.android");
            if(linkedIn != null) {
                initLinkedInAuthentication();
            }
            else{
                DialogUtil.showConfirmationDialog(getActivity(),
                        getActivity().getString(R.string.title_install_linkedin),
                        getActivity().getString(R.string.msg_install_linkedin),
                        getActivity().getString(R.string.label_download),
                        getActivity().getString(R.string.label_cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(which == DialogInterface.BUTTON_POSITIVE){
                                    getActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + "com.linkedin.android")));
                                }
                            }
                        });
            }

        }
    }

    private boolean areFieldsValid() {
        boolean areFieldsValid = true;

        //Check for field completion
        if (etEmail.getText().toString().trim().length() == 0) {
            areFieldsValid = false;
            String fieldName = getActivity().getString(R.string.label_email);
            String requiredFieldErrorMessage = getActivity().getString(R.string.error_required_field, fieldName);
            tilEmail.setError(requiredFieldErrorMessage);
        } else if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString().trim()).matches()) {
            areFieldsValid = false;
            String invalidEmailErrorMessage = getActivity().getString(R.string.error_invalid_email);
            tilEmail.setError(invalidEmailErrorMessage);
        }

        if (etPassword.getText().toString().trim().length() == 0) {
            areFieldsValid = false;
            String fieldName = getActivity().getString(R.string.label_password);
            String requiredFieldErrorMessage = getActivity().getString(R.string.error_required_field, fieldName);
            tilPassword.setError(requiredFieldErrorMessage);
        }

        return areFieldsValid;
    }

    private void logInUser() {

        // Close the soft keyboard
        if (getActivity().getCurrentFocus() != null && getActivity().getCurrentFocus().getWindowToken() != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }

        JSONObject jsonParams = null;
        try{
            jsonParams = createJsonParams(etEmail.getText().toString().trim(),
                    etPassword.getText().toString().trim());

            LoginRequest mRequest = new LoginRequest(getActivity(),
                    jsonParams,
                    this);

            dProgress = DialogUtility.showIndeterminateProgressDialog(getActivity(),
                    getActivity().getResources().getString(R.string.label_logging_in),
                    getActivity().getResources().getString(R.string.label_loading),
                    false);

            mRequest.execute();
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private JSONObject createJsonParams(String email, String password) throws JSONException {
        JSONObject jsonParams = new JSONObject();
        JSONObject jsonData = new JSONObject();

        jsonData.put("email", email);
        jsonData.put("password", password);

        String token = FirebaseInstanceId.getInstance().getToken();

        if(token != null && !token.isEmpty()){
            jsonData.put("notification_token", token);
            jsonData.put("device_type_id", AppConstants.API_DEVICE_TYPE_ID);
        }

        jsonParams.put("data", jsonData);

        return jsonParams;
    }

    private void initLinkedInAuthentication() {
        if (getActivity().getCurrentFocus() != null && getActivity().getCurrentFocus().getWindowToken() != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }

        if (getActivity() != null && getActivity() instanceof WelcomeActivity) {
            ((WelcomeActivity) getActivity()).initializeLinkedInAuthentication();
        }
    }

    public void onLinkedInSessionReady() {

        dProgress = DialogUtility.showIndeterminateProgressDialog(getActivity(),
                getActivity().getResources().getString(R.string.label_logging_in),
                getActivity().getResources().getString(R.string.label_loading),
                false);

        String url = AppConstants.linkedInUrlSimpleProfileFields();

        APIHelper apiHelper = APIHelper.getInstance(getActivity());
        apiHelper.getRequest(getActivity(), url, new ApiListener() {
            @Override
            public void onApiSuccess(ApiResponse apiResponse) {
                // Success!
                Log.d("~~", "onApiSuccess apiResponse: " + apiResponse.toString());
                Log.d("~~", "onApiSuccess apiResponse data: " + apiResponse.getResponseDataAsString());

                boolean responseHandled = false;

                JSONObject responseDataJson = apiResponse.getResponseDataAsJson();

                if (responseDataJson != null
                        && responseDataJson.has("emailAddress")
                        && responseDataJson.has("id")) {


                    String token = FirebaseInstanceId.getInstance().getToken();

                    if(token != null && !token.isEmpty()){
                        try {
                            responseDataJson.put("notification_token", token);
                            responseDataJson.put("device_type_id", AppConstants.API_DEVICE_TYPE_ID);

                            responseHandled = true;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                if(responseHandled){
                    logInUserViaLinkedIn(responseDataJson);
                }else{

                    if (dProgress != null && dProgress.isShowing()) {
                        dProgress.dismiss();
                    }

                    DialogUtil.showAlertDialog(getActivity(),
                            getActivity().getString(R.string.label_linkedin),
                            getActivity().getString(R.string.error_linkedin_auth));
                }
            }

            @Override
            public void onApiError(LIApiError liApiError) {
                Log.e(getFragment().getClassTag(), "LIAuthError: " + liApiError.toString());
                // Error making GET request!
                if (dProgress != null && dProgress.isShowing()) {
                    dProgress.dismiss();
                }

                DialogUtil.showAlertDialog(getActivity(),
                        getActivity().getString(R.string.label_linkedin),
                        getActivity().getString(R.string.error_linkedin_auth));
            }
        });
    }

    public void onLinkedInSessionError(String message) {
        if (dProgress != null && dProgress.isShowing()) {
            dProgress.dismiss();
        }

        if (message.isEmpty()) {
            message = getActivity().getString(R.string.error_linkedin_auth);
        }
        DialogUtil.showAlertDialog(getActivity(),
                getActivity().getString(R.string.label_linkedin),
                message);
    }


    private void logInUserViaLinkedIn(JSONObject userDetailJsonParams) {

        if (userDetailJsonParams != null) {

            LinkedInLogInRequest mRequest = new LinkedInLogInRequest(getActivity(),
                    userDetailJsonParams,
                    this);

            mRequest.execute();
        }
    }

    @Override
    public void onFailure(Call call, IOException e) {

        Log.d(getFragment().getClassTag(), "onFailure exception " + e.getMessage());

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (dProgress != null && dProgress.isShowing()) {
                    dProgress.dismiss();
                }

                DialogUtil.showAlertDialog(getActivity(),
                        getActivity().getString(R.string.label_error),
                        getActivity().getString(R.string.error_generic_error));
            }
        });
    }

    @Override
    public void onResponse(Call call, Response response) throws IOException {

        Log.d("~~", "onResponse response: " + response);

        if (dProgress != null && dProgress.isShowing()) {
            dProgress.dismiss();
        }

        if (response.isSuccessful()) {
            String responseBody = response.body().string();
            if (response.isSuccessful() && !responseBody.isEmpty() && !responseBody.equals("{}")) {
                Log.d("~~", "onResponse responseBody: " + responseBody);

                try {
                    JSONObject responseBodyJson = new JSONObject(responseBody);

                    if (responseBodyJson.has("status")) {
                        boolean statusOK = responseBodyJson.getBoolean("status");

                        if (statusOK) {
                            String dataResponse = responseBodyJson.getString("data");

                            Gson gson = new GsonBuilder().serializeNulls().create();
                            Type typeToken = new TypeToken<UserProfile>() {
                            }.getType();
                            UserProfile userProfile = gson.fromJson(dataResponse, typeToken);

                            if (userProfile != null) {
                                continueToSignUp(userProfile);
                            }

                        } else {
                            String messageResponse = responseBodyJson.getString("message");
                            if (messageResponse != null && !messageResponse.isEmpty()) {
                                showThreadSafeAlertDialog("", messageResponse, new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialogInterface) {
                                        etPassword.getText().clear();
                                    }
                                });
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    showThreadSafeAlertDialog(getActivity().getString(R.string.label_error),
                            getActivity().getString(R.string.error_generic_error));
                }
            }
        }
    }

    private void continueToSignUp(final UserProfile userProfile) {

        Log.d("~~", "continueToSignUp user profile: " + userProfile);

        if (!userProfile.getAuthenticationToken().isEmpty()) {
            Log.d("~~", "continueToSignUp save authtoken");
            SharedPreferencesHelper.saveAuthToken(getActivity(), userProfile.getAuthenticationToken());
            // And clear the salutations flag as this is a new user
            SharedPreferencesHelper.clearHasChosenSalutations(getActivity());
        }

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.putExtra(AppConstants.KEY_USER_PROFILE, userProfile);
                getActivity().switchActivity(intent, true);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onStop() {
        if (dProgress != null && dProgress.isShowing()) {
            dProgress.dismiss();
            dProgress = null;
        }
    }
}
