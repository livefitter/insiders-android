package com.livefitter.theinsiders.controller;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.widget.TextView;

import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.databinding.FragmentIcebreakerBinding;
import com.livefitter.theinsiders.model.Icebreaker;
import com.livefitter.theinsiders.request.RedeemIcebreakerRequest;
import com.livefitter.theinsiders.utility.DialogUtility;
import com.livefitter.theinsiders.view.SlideButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import ejdelrosario.framework.utilities.DialogUtil;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by LloydM on 3/7/17
 * for Livefitter
 */

public class IcebreakerController extends BaseController {

    private TextView tvSliderLabel;
    private SlideButton sbRedeem;
    private ProgressDialog dProgress;

    private FragmentIcebreakerBinding mBinding;
    private Icebreaker mIcebreaker;

    public IcebreakerController(BaseActivity activity, BaseFragment fragment) {
        super(activity, fragment);

        Bundle extras = getActivity().getIntent().getExtras();
        if (extras != null && extras.containsKey(AppConstants.KEY_ICEBREAKER)) {
            mIcebreaker = extras.getParcelable(AppConstants.KEY_ICEBREAKER);
        }
    }

    public void initialize(View rootView, Bundle savedInstanceState) {

        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        getActivity().setSupportActionBar(mToolbar);
        getActivity().getSupportActionBar().setTitle(R.string.title_icebreaker);
        getActivity().getSupportActionBar().setHomeButtonEnabled(true);

        tvSliderLabel = (TextView) rootView.findViewById(R.id.icebreaker_slider_label);

        sbRedeem = (SlideButton) rootView.findViewById(R.id.icebreaker_slider_handle);
        sbRedeem.setSlideButtonListener(new SlideButton.SlideButtonListener() {
            @Override
            public void onSlideStart() {
                ViewPropertyAnimator tvAnimator = tvSliderLabel.animate();

                tvAnimator.alpha(0).start();

                tvSliderLabel.setText("");
            }

            @Override
            public void onSlideRelease() {
                ViewPropertyAnimator tvAnimator = tvSliderLabel.animate();

                tvAnimator.alpha(1).start();

                tvSliderLabel.setText(R.string.label_slide_to_redeem);
            }

            @Override
            public void onSlideEnd() {
                ViewPropertyAnimator tvAnimator = tvSliderLabel.animate();

                tvAnimator.alpha(1).start();

                DialogUtil.showConfirmationDialog(getActivity(),
                        getActivity().getString(R.string.msg_confirm_redeem),
                        getActivity().getString(R.string.label_yes),
                        getActivity().getString(R.string.label_no),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(which == DialogInterface.BUTTON_POSITIVE){

                                    tvSliderLabel.setText(R.string.label_redeemeding);

                                    ViewPropertyAnimator sbAnimator = sbRedeem.animate();

                                    sbAnimator.alpha(0).start();

                                    redeemIcebreaker();

                                }
                            }
                        });

            }
        });

        if (mBinding != null && mIcebreaker != null) {
            mBinding.setIcebreaker(mIcebreaker);
        }
    }

    private void redeemIcebreaker() {
        dProgress = DialogUtility.showIndeterminateProgressDialog(getActivity(),
                "",
                getFragment().getString(R.string.label_redeemeding),
                false);

        RedeemIcebreakerRequest request = new RedeemIcebreakerRequest(getActivity(), new Callback() {

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (dProgress != null && dProgress.isShowing()) {
                    dProgress.dismiss();
                }

                if (response.isSuccessful()) {
                    String responseBody = response.body().string();
                    if (response.isSuccessful() && !responseBody.isEmpty() && !responseBody.equals("{}")) {
                        Log.d(getFragment().getClassTag(), "onResponse body: " + responseBody);

                        try {
                            JSONObject responseBodyJson = new JSONObject(responseBody);

                            if (responseBodyJson.has("status")
                                    && responseBodyJson.has("message")) {
                                final boolean statusOK = responseBodyJson.getBoolean("status");
                                final String messageResponse = responseBodyJson.getString("message");

                                if (messageResponse != null && !messageResponse.isEmpty()) {

                                    // Combine the two responses here
                                    // The only difference is if we're going to present an error message title or not

                                    final String messageTitle = statusOK ? "" : getFragment().getString(R.string.label_error);

                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (statusOK || messageResponse.contains("This image has been redeemed")) {
                                                mIcebreaker.setRedeemed(true);
                                                getActivity().setResult(AppCompatActivity.RESULT_OK);
                                            }

                                            DialogUtil.showAlertDialog(getActivity(),
                                                    messageTitle,
                                                    messageResponse);

                                            sbRedeem.setProgress(0);
                                        }
                                    });
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            showThreadSafeAlertDialog(
                                    getActivity().getString(R.string.label_error),
                                    getActivity().getString(R.string.error_generic_error),
                                    new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialogInterface) {
                                            resetSliderState();
                                        }
                                    });
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(getFragment().getClassTag(), "onFailure exception " + e.getMessage());

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (dProgress != null && dProgress.isShowing()) {
                            dProgress.dismiss();
                        }

                        DialogUtil.showAlertDialog(getActivity(),
                                getActivity().getString(R.string.label_error),
                                getActivity().getString(R.string.error_generic_error),
                                new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialogInterface) {
                                        resetSliderState();
                                    }
                                });

                    }
                });
            }
        });

        request.execute();
    }

    private void resetSliderState() {

        ViewPropertyAnimator tvAnimator = tvSliderLabel.animate();

        tvAnimator.alpha(0).start();
        tvSliderLabel.setText(R.string.label_slide_to_redeem);
        tvAnimator.alpha(1).start();

        ViewPropertyAnimator sbAnimator = sbRedeem.animate();

        sbAnimator.alpha(1).start();
    }

    public void setBinding(FragmentIcebreakerBinding binding) {
        this.mBinding = binding;
    }

    @Override
    public void onStop() {
        if (dProgress != null && dProgress.isShowing()) {
            dProgress.dismiss();
            dProgress = null;
        }
    }
}
