package com.livefitter.theinsiders.controller;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.activity.CrowdSourcedEventDetailActivity;
import com.livefitter.theinsiders.activity.EventDetailActivity;
import com.livefitter.theinsiders.activity.PaidEventDetailActivity;
import com.livefitter.theinsiders.adapter.PastEventsAdapter;
import com.livefitter.theinsiders.listener.EventClickListener;
import com.livefitter.theinsiders.model.EventModel;
import com.livefitter.theinsiders.request.RetrievePastEventsRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import ejdelrosario.framework.utilities.DialogUtil;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by LloydM on 3/7/17
 * for Livefitter
 */

public class PastEventsController extends BaseController
    implements EventClickListener{


    private TextView tvEmptyMessage;
    private SwipeRefreshLayout mRefreshLayout;
    private RecyclerView rvEventsList;

    private PastEventsAdapter mAdapter;

    public PastEventsController(BaseActivity activity, BaseFragment fragment){
        super(activity, fragment);
    }

    public void initialize(View rootView, Bundle savedInstanceState){
        tvEmptyMessage = (TextView) rootView.findViewById(R.id.events_empty_message);
        mRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.refresh_layout);
        rvEventsList = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        rvEventsList.setLayoutManager(mLayoutManager);

        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                retrieveEvents();

            }
        });

        retrieveEvents();
    }

    private void retrieveEvents() {

        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {

                mRefreshLayout.setRefreshing(true);

            }
        });

        RetrievePastEventsRequest request = new RetrievePastEventsRequest(getActivity(), new Callback() {

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {
                    String responseBody = response.body().string();
                    if (response.isSuccessful() && !responseBody.isEmpty() && !responseBody.equals("{}")) {
                        Log.d(getFragment().getClassTag(), "onResponse body: " + responseBody);

                        try {
                            JSONObject responseBodyJson = new JSONObject(responseBody);

                            if (responseBodyJson.has("status") && responseBodyJson.has("data")) {
                                boolean statusOK = responseBodyJson.getBoolean("status");

                                if (statusOK) {

                                    String dataResponse = responseBodyJson.getString("data");

                                    Gson gson = new Gson();
                                    Type typeToken = new TypeToken<ArrayList<EventModel>>() {}.getType();
                                    final ArrayList<EventModel> eventList = gson.fromJson(dataResponse, typeToken);

                                    if (eventList != null) {
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                handleResponse(eventList);
                                            }
                                        });
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            showThreadSafeAlertDialog(
                                    getActivity().getString(R.string.label_error),
                                    getActivity().getString(R.string.error_generic_error),
                                    new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialogInterface) {
                                            getActivity().onBackPressed();
                                        }
                                    });
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(getFragment().getClassTag(), "onFailure exception " + e.getMessage());

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        mRefreshLayout.post(new Runnable() {
                            @Override
                            public void run() {
                                mRefreshLayout.setRefreshing(false);
                            }
                        });

                        if(getFragment().isAdded()) {

                            Snackbar.make(getFragment().getView(),
                                    getActivity().getString(R.string.error_generic_error),
                                    Snackbar.LENGTH_SHORT).
                                    setAction(getActivity().getString(R.string.label_retry), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            retrieveEvents();
                                        }
                                    }).
                                    show();
                        }
                    }
                });
            }
        });

        request.execute();
    }

    private void handleResponse(ArrayList<EventModel> eventList) {

        mRefreshLayout.setRefreshing(false);

        if(eventList != null && !eventList.isEmpty()){
            tvEmptyMessage.setVisibility(View.INVISIBLE);

            mAdapter = new PastEventsAdapter(getActivity(), eventList, this);

            rvEventsList.setAdapter(mAdapter);
        }else{
            rvEventsList.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onEventClicked(EventModel eventModel) {

        if (eventModel != null) {

            switch (eventModel.getEventTypeId()){

                case AppConstants.EVENT_TYPE_NORMAL:{
                    //Normal Event
                    Intent intent = new Intent(getActivity(), EventDetailActivity.class);
                    intent.putExtra(AppConstants.KEY_EVENT_MODEL, eventModel);

                    getActivity().switchActivity(intent, false);

                    break;
                }

                case AppConstants.EVENT_TYPE_PAID:{
                    //Paid Event
                    Intent intent = new Intent(getActivity(), PaidEventDetailActivity.class);
                    intent.putExtra(AppConstants.KEY_EVENT_MODEL, eventModel);

                    getActivity().switchActivity(intent, false);

                    break;
                }

                case AppConstants.EVENT_TYPE_CROWD_SOURCED:{
                    //Crowd Sourced Event
                    Intent intent = new Intent(getActivity(), CrowdSourcedEventDetailActivity.class);
                    intent.putExtra(AppConstants.KEY_EVENT_MODEL, eventModel);

                    getActivity().switchActivity(intent, false);

                    break;
                }

            }
        }

    }

    @Override
    public void onStop() {

    }
}
