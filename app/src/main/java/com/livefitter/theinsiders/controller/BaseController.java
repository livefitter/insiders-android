package com.livefitter.theinsiders.controller;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ImageButton;

import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;

import ejdelrosario.framework.utilities.DialogUtil;

/**
 * Created by LloydM on 2/9/17
 * for Livefitter
 */

public abstract class BaseController {

    protected Toolbar mToolbar;
    protected BaseActivity mActivity;
    protected BaseFragment mFragment;

    public BaseController(BaseActivity activity, BaseFragment fragment) {
        this.mActivity = activity;
        this.mFragment = fragment;
    }

    public abstract void initialize(View rootView, Bundle savedInstanceState);

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        colorizeHomeButton();
    }

    public abstract void onStop();



    protected void showThreadSafeAlertDialog(final String title, final String message){

        if(getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtil.showAlertDialog(getActivity(),
                            title,
                            message);
                }
            });
        }
    }

    protected void showThreadSafeAlertDialog(final String title, final String message, final DialogInterface.OnDismissListener onDismissListener){

        if(getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtil.showAlertDialog(getActivity(),
                            title,
                            message,
                            onDismissListener);
                }
            });
        }
    }

    protected void colorizeHomeButton(){

        // Cheat way to colorize the hamburger icon without tweaking the colorPrimary etc
        // Color is based on "gold" color in color res
        int color = Color.parseColor("#dec666");
        final PorterDuffColorFilter colorFilter = new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP);

        for (int i = 0; i < mToolbar.getChildCount(); i++) {
            final View v = mToolbar.getChildAt(i);

            if (v instanceof ImageButton) {
                ((ImageButton) v).setColorFilter(colorFilter);
            }
        }
    }

    public BaseActivity getActivity() {
        return mActivity;
    }

    public BaseFragment getFragment() {
        return mFragment;
    }

}
