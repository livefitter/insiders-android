package com.livefitter.theinsiders.controller;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.model.EventModel;
import com.livefitter.theinsiders.request.ChangePasswordRequest;
import com.livefitter.theinsiders.utility.DialogUtility;
import com.livefitter.theinsiders.utility.EmojiExcludeFilter;
import com.livefitter.theinsiders.utility.FormFieldTextWatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import ejdelrosario.framework.utilities.DialogUtil;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by LloydM on 3/6/17
 * for Livefitter
 */

public class ChangePasswordController extends BaseController
        implements View.OnClickListener {

    private EventModel mEvent;

    private TextInputLayout tilCurrent, tilNew, tilConfirm;
    private TextInputEditText etCurrent, etNew, etConfirm;
    private Button btnUpdate;
    private ProgressDialog dProgress;

    public ChangePasswordController(BaseActivity activity, BaseFragment fragment) {
        super(activity, fragment);
    }

    @Override
    public void initialize(View rootView, Bundle savedInstanceState) {

        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        ((BaseActivity) getActivity()).setSupportActionBar(mToolbar);
        ((BaseActivity) getActivity()).enableUpbutton();

        etCurrent = (TextInputEditText) rootView.findViewById(R.id.password_edittext_current);
        etNew = (TextInputEditText) rootView.findViewById(R.id.password_edittext_new);
        etConfirm = (TextInputEditText) rootView.findViewById(R.id.password_edittext_confirm);

        InputFilter[] inputFilters = new InputFilter[]{new EmojiExcludeFilter()};
        etCurrent.setFilters(inputFilters);
        etNew.setFilters(inputFilters);
        etConfirm.setFilters(inputFilters);

        tilCurrent = (TextInputLayout) rootView.findViewById(R.id.password_edittext_layout_current);
        tilNew = (TextInputLayout) rootView.findViewById(R.id.password_edittext_layout_new);
        tilConfirm = (TextInputLayout) rootView.findViewById(R.id.password_edittext_layout_confirm);

        etCurrent.addTextChangedListener(new FormFieldTextWatcher(tilCurrent));
        etNew.addTextChangedListener(new FormFieldTextWatcher(tilNew));
        etConfirm.addTextChangedListener(new FormFieldTextWatcher(tilConfirm));

        etConfirm.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    // Close the soft keyboard
                    if(textView.getWindowToken() != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(textView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    }

                    if (areFieldsValid()) {
                        changePassword();
                        handled = true;
                    }
                }
                return handled;
            }
        });

        btnUpdate = (Button) rootView.findViewById(R.id.password_button_update);

        btnUpdate.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == btnUpdate) {
            // Close the soft keyboard
            if(getActivity().getCurrentFocus() != null && getActivity().getCurrentFocus().getWindowToken() != null) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }

            if (areFieldsValid()) {
                changePassword();
            }

        }
    }

    private boolean areFieldsValid() {
        boolean areFieldsValid = true;

        //Check for field completion
        if (etCurrent.getText().toString().trim().length() == 0) {
            areFieldsValid = false;
            String fieldName = getActivity().getString(R.string.label_current_password);
            String requiredFieldErrorMessage = getActivity().getString(R.string.error_required_field, fieldName);
            tilCurrent.setError(requiredFieldErrorMessage);
        }

        if (etNew.getText().toString().trim().length() == 0) {
            areFieldsValid = false;
            String fieldName = getActivity().getString(R.string.label_new_password);
            String requiredFieldErrorMessage = getActivity().getString(R.string.error_required_field, fieldName);
            tilNew.setError(requiredFieldErrorMessage);
        }

        if (etConfirm.getText().toString().trim().length() == 0) {
            areFieldsValid = false;
            String fieldName = getActivity().getString(R.string.label_confirm_password);
            String requiredFieldErrorMessage = getActivity().getString(R.string.error_required_field, fieldName);
            tilConfirm.setError(requiredFieldErrorMessage);
        }

        // Check if the new and confirm password fields match
        if (!etNew.getText().toString().trim().equals(etConfirm.getText().toString().trim())) {
            areFieldsValid = false;
            String requiredFieldErrorMessage = getActivity().getString(R.string.error_password_mismatch);
            tilConfirm.setError(requiredFieldErrorMessage);
        }

        return areFieldsValid;
    }

    private JSONObject createJsonParams() throws JSONException {
        JSONObject params = new JSONObject();

        JSONObject dataParams = new JSONObject();
        dataParams.put("current_password", etCurrent.getText().toString().trim());
        dataParams.put("password", etConfirm.getText().toString().trim());

        params.put("data", dataParams);

        return params;
    }

    private void changePassword() {
        JSONObject jsonParams = null;
        try {
            jsonParams = createJsonParams();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(jsonParams == null){
            return;
        }

        dProgress = DialogUtility.showIndeterminateProgressDialog(getActivity(),
                "",
                getFragment().getString(R.string.label_updating_password),
                false);

        ChangePasswordRequest request = new ChangePasswordRequest(getActivity(), jsonParams, new Callback() {

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.d(getFragment().getClassTag(), "onResponse: " +response);
                if (dProgress != null && dProgress.isShowing()) {
                    dProgress.dismiss();
                }

                if (response.isSuccessful()) {
                    String responseBody = response.body().string();
                    if (response.isSuccessful() && !responseBody.isEmpty() && !responseBody.equals("{}")) {
                        Log.d(getFragment().getClassTag(), "onResponse body: " + responseBody);

                        try {
                            JSONObject responseBodyJson = new JSONObject(responseBody);

                            if (responseBodyJson.has("status") && responseBodyJson.has("message")) {
                                boolean statusOK = responseBodyJson.getBoolean("status");
                                String messageResponse = responseBodyJson.getString("message");

                                if (statusOK) {

                                    if (messageResponse != null && !messageResponse.isEmpty()) {
                                        showThreadSafeAlertDialog("",
                                                messageResponse,
                                                new DialogInterface.OnDismissListener() {
                                                    @Override
                                                    public void onDismiss(DialogInterface dialogInterface) {
                                                        getActivity().finish();
                                                    }
                                                });
                                    }
                                } else {


                                    if (messageResponse != null && !messageResponse.isEmpty()) {
                                        showThreadSafeAlertDialog(getActivity().getString(R.string.label_error),
                                                messageResponse,
                                                new DialogInterface.OnDismissListener() {
                                                    @Override
                                                    public void onDismiss(DialogInterface dialogInterface) {
                                                        clearFields();
                                                    }
                                                });
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            showThreadSafeAlertDialog(
                                    getActivity().getString(R.string.label_error),
                                    getActivity().getString(R.string.error_generic_error));
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(getFragment().getClassTag(), "onFailure exception " + e.getMessage());

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (dProgress != null && dProgress.isShowing()) {
                            dProgress.dismiss();
                        }

                        DialogUtil.showAlertDialog(getActivity(),
                                getActivity().getString(R.string.label_error),
                                getActivity().getString(R.string.error_generic_error));

                    }
                });
            }
        });

        request.execute();
    }

    private void clearFields() {
        etCurrent.getText().clear();
        etNew.getText().clear();
        etConfirm.getText().clear();
    }

    @Override
    public void onStop() {
        if(dProgress != null && dProgress.isShowing()){
            dProgress.dismiss();
            dProgress = null;
        }
    }
}
