package com.livefitter.theinsiders.controller;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.fragment.CreditCardListFragment;
import com.livefitter.theinsiders.model.EventModel;
import com.livefitter.theinsiders.model.Guest;
import com.livefitter.theinsiders.model.PromoCode;
import com.livefitter.theinsiders.request.ValidatePromoCodeRequest;
import com.livefitter.theinsiders.utility.EmojiExcludeFilter;
import com.livefitter.theinsiders.utility.FormFieldTextWatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by LFT-PC-010 on 7/14/2017.
 */

public class PriceSummaryController extends BaseController {
    private static final String TAG = PriceSummaryController.class.getSimpleName();

    private TextView tvEventName, tvPrice, tvQuantity, tvTotal, tvZeroGuests, tvPromoLabel, tvPromoAmount;
    private RecyclerView rvGuests;
    private LinearLayout llPromoInput, llPromoAmount;
    private TextInputLayout tilPromoCode;
    private TextInputEditText etPromoCode;

    private ProgressDialog pdLoading;

    private EventModel mEventModel;
    private ArrayList<Guest> invitedGuests;
    private PromoCode promoCode;

    public PriceSummaryController(BaseActivity activity, BaseFragment fragment) {
        super(activity, fragment);
    }

    @Override
    public void initialize(View rootView, Bundle savedInstanceState) {

        mToolbar = rootView.findViewById(R.id.toolbar);
        getActivity().setSupportActionBar(mToolbar);
        getActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvEventName = rootView.findViewById(R.id.check_out_textview_event_name);
        tvPrice = rootView.findViewById(R.id.check_out_textview_price);
        tvQuantity = rootView.findViewById(R.id.check_out_textview_quantity);
        tvTotal = rootView.findViewById(R.id.check_out_textview_total);
        tvZeroGuests = rootView.findViewById(R.id.check_out_textview_zero_invites);
        tvPromoLabel = rootView.findViewById(R.id.check_out_textview_promo_label);
        tvPromoAmount = rootView.findViewById(R.id.check_out_textview_promo_discount);

        tilPromoCode = rootView.findViewById(R.id.check_out_edittext_promo_layout);
        etPromoCode = rootView.findViewById(R.id.check_out_edittext_promo);
        etPromoCode.addTextChangedListener(new FormFieldTextWatcher(tilPromoCode));
        etPromoCode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_GO) {
                    validatePromoCode();
                    return true;
                }
                return false;
            }
        });
        etPromoCode.setFilters(new InputFilter[]{new EmojiExcludeFilter()});

        llPromoInput = rootView.findViewById(R.id.check_out_promo_input_layout);
        llPromoAmount = rootView.findViewById(R.id.check_out_promo_amount_layout);

        rvGuests = rootView.findViewById(R.id.check_out_recyclerview_guest);

        mEventModel = getFragment().getArguments().getParcelable(AppConstants.KEY_EVENT_MODEL);
        invitedGuests = getFragment().getArguments().getParcelableArrayList(AppConstants.KEY_INVITED_GUEST_LIST);

        setupValues();

        rootView.findViewById(R.id.check_out_button_proceed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CreditCardListFragment creditCardListFragment = new CreditCardListFragment();

                //add other needed arguments for this fragment
                getFragment().getArguments().putBoolean(AppConstants.KEY_CHECK_OUT_MODE, true);
                if (promoCode != null) {
                    getFragment().getArguments().putParcelable(AppConstants.KEY_PROMO_CODE, promoCode);
                }
                //just pass the same arguments since the same data is needed
                creditCardListFragment.setArguments(getFragment().getArguments());

                getActivity().switchFragment(
                        creditCardListFragment,
                        R.id.checkout_fragment_container,
                        AppConstants.FRAG_CREDIT_CARD_LIST,
                        true,
                        false);

            }
        });

        rootView.findViewById(R.id.check_out_button_apply_promo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validatePromoCode();
            }
        });
    }

    private void setupValues() {

        float priceFloat = Float.valueOf(mEventModel.getPrice());
        int quantity = invitedGuests.size() + (mEventModel.getHasRsvped() ? 0 : 1);

        float total = quantity * priceFloat;

        tvEventName.setText(mEventModel.getTitle());

        tvPrice.setText(String.format("$%.2f", priceFloat));

        tvQuantity.setText("x" + quantity);


        tvZeroGuests.setVisibility(invitedGuests.size() > 0 ? View.INVISIBLE : View.VISIBLE);

        rvGuests.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvGuests.setHasFixedSize(false);
        rvGuests.setAdapter(new GuestListAdapter());

        if (promoCode != null) {
            Log.d(TAG, "setupValues: promoCode " + promoCode);
            llPromoInput.setVisibility(View.INVISIBLE);
            llPromoAmount.setVisibility(View.VISIBLE);

            String promoAmountLabel = getActivity().getString(R.string.label_promo_code_amount, promoCode.getDiscount());
            //todo error at this line: java.util.UnknownFormatConversionException: Conversion = ')'
            //getActivity().getString(R.string.label_promo_code_amount);
            tvPromoLabel.setText(promoAmountLabel);

            float discountedAmount = total * ((promoCode.getDiscount() * 1f) / 100f);
            // Remove this amount from the computed total
            total = total - discountedAmount;

            String discountLabel = getActivity().getString(R.string.label_promo_amount, String.format("$%.2f", discountedAmount));
            tvPromoAmount.setText(discountLabel);
        } else {
            llPromoInput.setVisibility(View.VISIBLE);
            llPromoAmount.setVisibility(View.INVISIBLE);

            etPromoCode.getText().clear();
        }

        tvTotal.setText(String.format("$%.2f", total));
    }

    private void validatePromoCode() {

        if (getFragment().getActivity() instanceof BaseActivity) {
            ((BaseActivity) getFragment().getActivity()).hideSoftKeyboard();
        }

        if (etPromoCode.getText().length() > 0) {
            doValidatePromoCode(etPromoCode.getText().toString());
        } else {
            String fieldName = getActivity().getString(R.string.label_promo_code);
            String message = getActivity().getString(R.string.error_required_field, fieldName);
            tilPromoCode.setErrorEnabled(true);
            tilPromoCode.setError(message);
        }
    }

    private void doValidatePromoCode(final String promoCodeString) {

        showProgressDialog(getActivity().getString(R.string.label_validating_promo_code));

        ValidatePromoCodeRequest request = new ValidatePromoCodeRequest(getActivity(), promoCodeString, mEventModel.getId(), new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                hideProgressDialog();

                showThreadSafeAlertDialog(getActivity().getString(R.string.label_error),
                        getActivity().getString(R.string.error_generic_error));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                hideProgressDialog();

                String errorMessage = null;

                if (response.isSuccessful()) {

                    try {

                        JSONObject jobj = new JSONObject(response.body().string());

                        boolean statusOk = jobj.getBoolean("status");

                        if (statusOk) {

                            Gson gson = new Gson();
                            Type type = new TypeToken<PromoCode>() {
                            }.getType();

                            promoCode = gson.fromJson(jobj.getJSONObject("data").toString(), type);
                            Log.d("~~", "onResponse: promoCode " + promoCode);

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    setupValues();
                                }
                            });

                        } else {

                            errorMessage = jobj.getString("message");

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        errorMessage = getActivity().getString(R.string.error_generic_error);
                    }

                } else {

                    errorMessage = getActivity().getString(R.string.error_generic_error);

                }

                if (errorMessage != null) {

                    showThreadSafeAlertDialog(getActivity().getString(R.string.label_error), errorMessage);

                }

            }

        });

        request.execute();
    }

    @Override
    public void onStop() {

    }

    private void showProgressDialog(String message) {

        if (pdLoading == null) {

            pdLoading = new ProgressDialog(getActivity());
            pdLoading.setCancelable(false);

        }

        pdLoading.setMessage(message);

        if (!pdLoading.isShowing()) {
            pdLoading.show();
        }

    }

    private void hideProgressDialog() {

        if (pdLoading != null && pdLoading.isShowing()) {

            pdLoading.dismiss();

        }

    }

    private class GuestListAdapter extends RecyclerView.Adapter<GuestListViewHolder> {


        @Override
        public GuestListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new GuestListViewHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_invited_guest, parent, false));
        }

        @Override
        public void onBindViewHolder(GuestListViewHolder holder, int position) {

            holder.tvGuestName.setText(invitedGuests.get(position).getName());

        }

        @Override
        public int getItemCount() {
            return invitedGuests == null ? 0 : invitedGuests.size();
        }
    }

    private class GuestListViewHolder extends RecyclerView.ViewHolder {

        TextView tvGuestName;

        public GuestListViewHolder(View itemView) {
            super(itemView);
            tvGuestName = (TextView) itemView.findViewById(R.id.check_out_textview_guest_name);
        }
    }
}
