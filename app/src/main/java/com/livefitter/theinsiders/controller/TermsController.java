package com.livefitter.theinsiders.controller;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.request.FaqTermsRequest;
import com.livefitter.theinsiders.utility.DialogUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import ejdelrosario.framework.utilities.DialogUtil;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by LFT-PC-010 on 6/22/2017.
 */

public class TermsController extends BaseController {

    private TextView tvContent;

    public TermsController(BaseActivity activity, BaseFragment fragment) {
        super(activity, fragment);
    }

    @Override
    public void initialize(View rootView, Bundle savedInstanceState) {

        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        getActivity().setSupportActionBar(mToolbar);
        getActivity().getSupportActionBar().setTitle(getActivity().getString(R.string.label_terms));
        getActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvContent = (TextView) rootView.findViewById(R.id.faq_terms_textview_content);

        getFaqContent();
    }

    private void getFaqContent(){

        final ProgressDialog pdLoading = DialogUtility.showIndeterminateProgressDialog(getActivity(),
                getActivity().getString(R.string.label_terms),
                getActivity().getString(R.string.label_loading),
                false);

        FaqTermsRequest request = new FaqTermsRequest(getActivity(), new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                if(pdLoading != null && pdLoading.isShowing()){
                    pdLoading.dismiss();
                }

                DialogUtil.showAlertDialog(getActivity(),
                        getActivity().getString(R.string.label_error),
                        getActivity().getString(R.string.error_generic_error), new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                getActivity().onBackPressed();
                            }
                        });

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if(pdLoading != null && pdLoading.isShowing()){
                    pdLoading.dismiss();
                }

                String errorMessage = null;

                try {

                    JSONObject jobj = new JSONObject(response.body().string());

                    boolean status = jobj.getBoolean("status");

                    if(status){

                        setContent(jobj.getJSONObject("data").getString("terms_and_condition"));

                    }
                    else{

                        errorMessage = jobj.getString("message");

                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                    errorMessage = getActivity().getString(R.string.error_generic_error);

                }

                if(errorMessage != null){

                    showThreadSafeAlertDialog(getActivity().getString(R.string.label_error),
                            errorMessage,
                            new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    getActivity().onBackPressed();
                                }
                            });

                }

            }
        });

        request.execute();

    }

    private void setContent(final String content){

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                tvContent.setText(content);

            }
        });

    }

    @Override
    public void onStop() {

    }
}
