package com.livefitter.theinsiders.controller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.activity.CrowdSourcedEventDetailActivity;
import com.livefitter.theinsiders.activity.EventDetailActivity;
import com.livefitter.theinsiders.activity.MainActivity;
import com.livefitter.theinsiders.activity.PaidEventDetailActivity;
import com.livefitter.theinsiders.adapter.HomeEventPagerAdapter;
import com.livefitter.theinsiders.listener.EventClickListener;
import com.livefitter.theinsiders.model.EventModel;
import com.livefitter.theinsiders.request.HomeEventRequest;
import com.livefitter.theinsiders.utility.DialogUtility;
import com.livefitter.theinsiders.view.HomeEventPageTransformer;
import com.livefitter.theinsiders.view.LoopingUnderlinePageIndicator;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import ejdelrosario.framework.utilities.DialogUtil;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by LloydM on 2/1/17
 * for Livefitter
 */

public class HomeController extends BaseController implements EventClickListener, Callback {

    private boolean shouldRetrieveEvents = true;

    private ViewPager vpEvent;
    private LoopingUnderlinePageIndicator mPageIndicator;
    private HomeEventPagerAdapter mPagerAdapter;
    private LinearLayout rlNoEvent;
    private ProgressDialog dProgress;

    //private EditText tvFirebaseDebug;

    private HomeEventRequest mRequest;

    public HomeController(BaseActivity activity, BaseFragment fragment) {
        super(activity, fragment);

        mPagerAdapter = new HomeEventPagerAdapter(getActivity(), this);
    }

    public void initialize(View rootView, Bundle savedInstanceState) {

        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        MainActivity homeActivity = (MainActivity) getActivity();
        homeActivity.setSupportActionBar(mToolbar);
        homeActivity.getSupportActionBar().setTitle(R.string.title_home);
        homeActivity.setupDrawerWithToolbar(mToolbar);
        homeActivity.getSupportActionBar().setHomeButtonEnabled(true);

        rlNoEvent = (LinearLayout) rootView.findViewById(R.id.home_layout_no_event);
        rlNoEvent.setVisibility(View.INVISIBLE);

        vpEvent = (ViewPager) rootView.findViewById(R.id.viewpager);
        vpEvent.setOffscreenPageLimit(2);
        vpEvent.setPageTransformer(false, new HomeEventPageTransformer());
        vpEvent.setAdapter(mPagerAdapter);

        mPageIndicator = (LoopingUnderlinePageIndicator) rootView.findViewById(R.id.home_pager_indicator);
        mPageIndicator.setViewPager(vpEvent);
    }

    public void retrieveAllEvents() {
        if (shouldRetrieveEvents) {
            mRequest = new HomeEventRequest(getActivity(), this);
            dProgress = DialogUtility.showIndeterminateProgressDialog(getActivity(),
                    "",
                    getActivity().getResources().getString(R.string.label_retrieving_events),
                    false);
            mRequest.execute();
        }
    }

    @Override
    public void onResponse(Call call, Response response) throws IOException {
        if (dProgress != null && dProgress.isShowing()) {
            dProgress.dismiss();
        }

        if (response.isSuccessful()) {
            String responseBody = response.body().string();
            if (response.isSuccessful() && !responseBody.isEmpty() && !responseBody.equals("{}")) {
                Log.d(mFragment.getClassTag(), "onResponse body: " + responseBody);

                try {
                    JSONObject responseBodyJson = new JSONObject(responseBody);

                    if (responseBodyJson.has("status") && responseBodyJson.has("data")) {
                        shouldRetrieveEvents = false;
                        boolean statusOK = responseBodyJson.getBoolean("status");

                        if (statusOK) {
                            String dataResponse = responseBodyJson.getString("data");
                            if (dataResponse != null && !dataResponse.isEmpty()) {
                                handleSuccessfulResponse(dataResponse);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    showThreadSafeAlertDialog(
                            getActivity().getString(R.string.label_error),
                            getActivity().getString(R.string.error_generic_error));
                }
            }
        }
    }

    @Override
    public void onFailure(Call call, IOException e) {

        Log.e(mFragment.getClassTag(), "onFailure exception " + e.getMessage());

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (dProgress != null && dProgress.isShowing()) {
                    dProgress.dismiss();
                }

                DialogUtil.showAlertDialog(getActivity(),
                        getActivity().getString(R.string.label_error),
                        getActivity().getString(R.string.error_generic_error));
            }
        });
    }

    private void handleSuccessfulResponse(String response) {

        Gson gson = new Gson();
        Type eventListType = new TypeToken<ArrayList<EventModel>>() {
        }.getType();
        final ArrayList<EventModel> eventList = gson.fromJson(response, eventListType);

        // Run on UI thread for our view-related changes
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (eventList != null && !eventList.isEmpty()) {

                        rlNoEvent.setVisibility(View.INVISIBLE);
                        vpEvent.setVisibility(View.VISIBLE);
                        mPagerAdapter.setEventList(eventList);

                        int FIRST_PAGE = eventList.size() * HomeEventPagerAdapter.LOOPS / 2;
                        // Set current item to the middle page so we can fling to both
                        // directions left and right
                        //vpEvent.setCurrentItem(FIRST_PAGE);
                        mPageIndicator.setCurrentItem(FIRST_PAGE);
                    }else{
                        vpEvent.setVisibility(View.INVISIBLE);
                        rlNoEvent.setVisibility(View.VISIBLE);
                    }
                }
            });

    }

    @Override
    public void onEventClicked(EventModel eventModel) {
        Log.d(mFragment.getClassTag(), "onEventClicked eventModel: " + eventModel);

        if (eventModel != null) {

            switch (eventModel.getEventTypeId()){

                case AppConstants.EVENT_TYPE_NORMAL:{
                    //Normal Event
                    Intent intent = new Intent(getActivity(), EventDetailActivity.class);
                    intent.putExtra(AppConstants.KEY_EVENT_MODEL, eventModel);

                    getActivity().switchActivity(intent, false);

                    break;
                }

                case AppConstants.EVENT_TYPE_PAID:{
                    //Paid Event
                    Intent intent = new Intent(getActivity(), PaidEventDetailActivity.class);
                    intent.putExtra(AppConstants.KEY_EVENT_MODEL, eventModel);

                    getActivity().switchActivity(intent, false);

                    break;
                }

                case AppConstants.EVENT_TYPE_CROWD_SOURCED:{
                    //Crowd Sourced Event
                    Intent intent = new Intent(getActivity(), CrowdSourcedEventDetailActivity.class);
                    intent.putExtra(AppConstants.KEY_EVENT_MODEL, eventModel);

                    getActivity().switchActivity(intent, false);

                    break;
                }

            }
        }
    }

    public boolean onBackPressed() {
        return true;
    }

    @Override
    public void onStop() {
        if (dProgress != null && dProgress.isShowing()) {
            dProgress.dismiss();
            dProgress = null;
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        int color = Color.parseColor("#dec666");
        final PorterDuffColorFilter colorFilter = new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP);

        for (int i = 0; i < mToolbar.getChildCount(); i++) {
            final View v = mToolbar.getChildAt(i);

            if (v instanceof ImageButton) {
                ((ImageButton) v).setColorFilter(colorFilter);
            }
        }

        inflater.inflate(R.menu.menu_home, menu);
    }

    public boolean onOptionsItemSelected(int id){

        if(id == R.id.action_refresh){
            shouldRetrieveEvents = true;
            retrieveAllEvents();
            return true;
        }

        return false;
    }
}
