package com.livefitter.theinsiders.controller;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.view.View;
import android.widget.Toast;

import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.activity.FaqActivity;
import com.livefitter.theinsiders.activity.MainActivity;
import com.livefitter.theinsiders.activity.TermsActivity;
import com.livefitter.theinsiders.request.SendMessageRequest;
import com.livefitter.theinsiders.utility.DialogUtility;
import com.livefitter.theinsiders.utility.EmojiExcludeFilter;
import com.livefitter.theinsiders.utility.FormFieldTextWatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import ejdelrosario.framework.utilities.DialogUtil;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by LFT-PC-010 on 6/21/2017.
 */

public class SupportController extends BaseController {

    private TextInputLayout tilSubject, tilMessage;
    private TextInputEditText etSubject, etMessage;

    public SupportController(BaseActivity activity, BaseFragment fragment) {
        super(activity, fragment);
    }

    @Override
    public void initialize(View rootView, Bundle savedInstanceState) {

        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        getActivity().setSupportActionBar(mToolbar);
        getActivity().setTitle(getActivity().getString(R.string.title_support));
        ((MainActivity) getActivity()).setupDrawerWithToolbar(mToolbar);
        getActivity().getSupportActionBar().setHomeButtonEnabled(true);

        tilSubject  = (TextInputLayout) rootView.findViewById(R.id.support_edittext_subject_layout);
        tilMessage  = (TextInputLayout) rootView.findViewById(R.id.support_edittext_message_layout);
        etSubject   = (TextInputEditText) rootView.findViewById(R.id.support_edittext_subject);
        etMessage   = (TextInputEditText) rootView.findViewById(R.id.support_edittext_message);

        InputFilter[] inputFilters = new InputFilter[]{new EmojiExcludeFilter()};

        etSubject.addTextChangedListener(new FormFieldTextWatcher(tilSubject));
        etSubject.setFilters(inputFilters);
        etMessage.addTextChangedListener(new FormFieldTextWatcher(tilMessage));
        etMessage.setFilters(inputFilters);

        //submit button
        rootView.findViewById(R.id.support_button_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                submitMessage();

            }
        });

        //FAQ button
        rootView.findViewById(R.id.support_button_faq).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().switchActivity(FaqActivity.class, false);

            }
        });

        //Terms button
        rootView.findViewById(R.id.support_button_terms).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().switchActivity(TermsActivity.class, false);

            }
        });

    }

    private void submitMessage(){

        if(isValid()){

            final ProgressDialog pdLoading = DialogUtility.showIndeterminateProgressDialog(getActivity(),
                    getActivity().getString(R.string.title_support),
                    getActivity().getString(R.string.label_sending_message),
                    false);


            try{

                JSONObject jobjParams = createJsonParams();

                SendMessageRequest request = new SendMessageRequest(getActivity(), jobjParams, new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {

                        if(pdLoading != null && pdLoading.isShowing()){
                            pdLoading.dismiss();
                        }

                        DialogUtil.showAlertDialog(getActivity(),
                                getActivity().getString(R.string.label_error),
                                getActivity().getString(R.string.error_generic_error));

                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {

                        if(pdLoading != null && pdLoading.isShowing()){
                            pdLoading.dismiss();
                        }

                        String errorMessage = null;

                        try {

                            JSONObject jobj = new JSONObject(response.body().string());

                            boolean status = jobj.getBoolean("status");

                            if(!status){
                                errorMessage = jobj.getString("message");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            errorMessage = getActivity().getString(R.string.error_generic_error);
                        }

                        if (errorMessage != null){

                            showThreadSafeAlertDialog(getActivity().getString(R.string.label_error), errorMessage);

                        }
                        else{

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    etSubject.setText("");
                                    etMessage.setText("");
                                    DialogUtil.showToast(getActivity(),
                                            getActivity().getString(R.string.label_message_sent),
                                            Toast.LENGTH_LONG);
                                }
                            });

                        }

                    }
                });

                request.execute();

            } catch (JSONException e) {
                e.printStackTrace();

                if(pdLoading != null && pdLoading.isShowing()){
                    pdLoading.dismiss();
                }

                DialogUtil.showAlertDialog(getActivity(),
                        getActivity().getString(R.string.label_error),
                        getActivity().getString(R.string.error_generic_error));
            }
        }

    }

    private boolean isValid(){

        boolean isValid = true;

        if(etSubject.getText().toString().trim().length() <= 0){

            String fieldName = getActivity().getString(R.string.label_subject);
            String requiredFieldErrorMessage = getActivity().getString(R.string.error_required_field, fieldName);
            tilSubject.setError(requiredFieldErrorMessage);
            isValid = false;
        }

        if(etMessage.getText().toString().trim().length() <= 0){

            String fieldName = getActivity().getString(R.string.label_message);
            String requiredFieldErrorMessage = getActivity().getString(R.string.error_required_field, fieldName);
            tilMessage.setError(requiredFieldErrorMessage);
            isValid = false;

        }

        return isValid;

    }

    private JSONObject createJsonParams() throws JSONException {

        JSONObject jobj = new JSONObject();
        JSONObject jobjData = new JSONObject();

        jobjData.put("title", etSubject.getText().toString());
        jobjData.put("message", etMessage.getText().toString());

        jobj.put("data", jobjData);

        return jobj;

    }

    @Override
    public void onStop() {

    }
}
