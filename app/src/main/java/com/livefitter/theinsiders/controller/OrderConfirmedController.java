package com.livefitter.theinsiders.controller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.activity.RsvpActivity;
import com.livefitter.theinsiders.model.EventModel;
import com.livefitter.theinsiders.model.Guest;
import com.livefitter.theinsiders.request.RetrieveOrderNumberRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by LFT-PC-010 on 7/18/2017.
 */

public class OrderConfirmedController extends BaseController {

    private TextView tvOrderNumber;
    private Button btnViewInvites;
    private ProgressDialog pdLoading;

    public OrderConfirmedController(BaseActivity activity, BaseFragment fragment) {
        super(activity, fragment);
    }

    @Override
    public void initialize(View rootView, Bundle savedInstanceState) {

        tvOrderNumber = (TextView) rootView.findViewById(R.id.check_out_textview_order_number);

        btnViewInvites = (Button) rootView.findViewById(R.id.check_out_button_view_invites);

        final EventModel mEventModel = getFragment().getArguments().getParcelable(AppConstants.KEY_EVENT_MODEL);
        final ArrayList<Guest> invitedGuests = getFragment().getArguments().getParcelableArrayList(AppConstants.KEY_INVITED_GUEST_LIST);

        if(invitedGuests.size() <= 0){

            btnViewInvites.setText(getActivity().getString(R.string.label_done));

        }

        btnViewInvites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(invitedGuests.size() <= 0){

                    getActivity().finish();

                }
                else {

                    Intent intent = new Intent(getActivity(), RsvpActivity.class);
                    intent.putExtra(AppConstants.KEY_EVENT_MODEL, mEventModel);
                    getActivity().switchActivity(intent, true);

                }

            }
        });

        retrieveOrderNumber();

    }

    @Override
    public void onStop() {

    }

    private void showProgressDialog(String message){

        if(pdLoading == null){

            pdLoading = new ProgressDialog(getActivity());
            pdLoading.setCancelable(false);

        }

        pdLoading.setMessage(message);

        if(!pdLoading.isShowing()) {
            pdLoading.show();
        }

    }

    private void hideProgressDialog(){

        if(pdLoading != null && pdLoading.isShowing()){

            pdLoading.dismiss();

        }

    }

    private void retrieveOrderNumber() {

        showProgressDialog(getActivity().getString(R.string.label_retrieving_order_number));

        RetrieveOrderNumberRequest request = new RetrieveOrderNumberRequest(getActivity(), new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                hideProgressDialog();

                showThreadSafeAlertDialog(getActivity().getString(R.string.label_error),
                        getActivity().getString(R.string.error_generic_error));

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                hideProgressDialog();

                String errorMessage = null;

                if(response.isSuccessful()){

                    try {

                        JSONObject jobj = new JSONObject(response.body().string());

                        boolean statusOk = jobj.getBoolean("status");

                        if(statusOk){

                            setOrderNumber(jobj.getJSONObject("data").getString("order_number"));

                        }
                        else{

                            errorMessage = jobj.getString("message");

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        errorMessage = getActivity().getString(R.string.error_generic_error);
                    }

                }
                else{

                    errorMessage = getActivity().getString(R.string.error_generic_error);

                }

                if(errorMessage != null){

                    showThreadSafeAlertDialog(getActivity().getString(R.string.label_error), errorMessage);

                }

            }
        });

        request.execute();

    }

    private void setOrderNumber(final String orderNumber) {

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                tvOrderNumber.setText(getActivity().getString(R.string.label_order_number, orderNumber));

            }
        });
    }
}
