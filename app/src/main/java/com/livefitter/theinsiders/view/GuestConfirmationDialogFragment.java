package com.livefitter.theinsiders.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;

import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.model.Guest;

import java.util.ArrayList;


/**
 * Created by LloydM on 2/3/17
 * for Livefitter
 */

public class GuestConfirmationDialogFragment extends AppCompatDialogFragment {

    // Determine if we are going to include the user in RSVP for the event
    // Change the message of this dialog if we are RSVP-ing for the user
    // and if we are inviting some guests, if any.
    private boolean rsvpUser;

    private ArrayList<Guest> guestList;

    private DialogInterface.OnClickListener clickListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        setCancelable(true);

        String message = "";

        if(rsvpUser && guestList.isEmpty()) {
            // RSVP for user only
            message = getActivity().getString(R.string.msg_rsvp_event);
        }else if(rsvpUser && !guestList.isEmpty()){
            // RSVP for user and invite guests
            message = getActivity().getString(R.string.msg_rsvp_event_with_guests) + concatinateGuestList();
        }else{
            // Inviting guests only
            message = getActivity().getString(R.string.msg_confirm_guests) + concatinateGuestList();
        }

        Log.d("~~", "onCreateDialog message: " + message);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.title_rsvp)
                .setMessage(message)
                .setPositiveButton(R.string.label_yes, clickListener)
                .setNegativeButton(R.string.label_no, clickListener);


        return builder.create();
    }

    private String concatinateGuestList(){

        String message = "\n\n";

        for (int i = 0; i < guestList.size(); i++) {
            Guest guest = guestList.get(i);

            message = message.concat(guest.getWholeName());

            if (i < guestList.size() - 1) {
                message = message.concat(", \n");
            }
        }

        return message;
    }

    public void setRsvpUser(boolean rsvpUser){
        this.rsvpUser = rsvpUser;
    }

    public void setGuestList(ArrayList<Guest> guestList) {
        this.guestList = guestList;
    }

    public void setClickListener(DialogInterface.OnClickListener clickListener) {
        this.clickListener = clickListener;
    }
}
