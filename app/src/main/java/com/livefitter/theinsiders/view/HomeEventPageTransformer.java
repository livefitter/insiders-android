package com.livefitter.theinsiders.view;

import android.annotation.TargetApi;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;


/**
 * Created by LloydM on 1/31/17
 * for Livefitter
 */

public class HomeEventPageTransformer implements ViewPager.PageTransformer {

    // Scaling values. Change MAX_SCALE to tweak width of views
    private static final float MAX_SCALE = 0.82f;
    private static final float MIN_SCALE = 0.5f;

    // Alpha values. Maximum is 1.
    private static final float MIN_ALPHA = 0.6f;

    @Override
    public void transformPage(View view, float position) {
        int pageWidth = view.getWidth();

        if(position >= -1 || position <= 1){
            // Left page, Center page, and Right page
            //float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));
            float scaleFactor = MAX_SCALE - Math.abs(position);

            int translationX = Math.round((pageWidth * -position) / 3);
            view.setTranslationX(translationX);


            // Scale the page down (between MIN_SCALE and 1)
            if(scaleFactor > 1){
                scaleFactor = 1;
            }else if(scaleFactor < MIN_SCALE){
                scaleFactor = MIN_SCALE;
            }
            view.setScaleX(scaleFactor);
            view.setScaleY(scaleFactor);

            float alpha = (1 - Math.abs(position));
            if(alpha > 1){
                alpha = 1;
            }else if(alpha < MIN_ALPHA){
                alpha = MIN_ALPHA;
            }
            view.setAlpha(alpha);


        }else if(position < -1 || position > 1){
            // pages N + 1 from left or right
            view.setAlpha(0);
        }
    }
}
