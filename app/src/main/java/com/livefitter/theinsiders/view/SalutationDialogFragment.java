package com.livefitter.theinsiders.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.livefitter.theinsiders.R;


/**
 * Created by LloydM on 2/3/17
 * for Livefitter
 */

public class SalutationDialogFragment extends AppCompatDialogFragment implements OnClickListener{

    Button btnMister, btnMiss, btnMisis;

    OnSalutationSelectedListener listener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        View root = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_salutation, null, false);

        btnMister = (Button) root.findViewById(R.id.button_salutation_mister);
        btnMiss = (Button) root.findViewById(R.id.button_salutation_miss);
        btnMisis = (Button) root.findViewById(R.id.button_salutation_misis);

        btnMister.setOnClickListener(this);
        btnMiss.setOnClickListener(this);
        btnMisis.setOnClickListener(this);

        setCancelable(false);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(root);

        return builder.create();
    }

    @Override
    public void onClick(View view) {
        if(view == btnMister){
            listener.onSalutationSelected(getString(R.string.salutations_mister));
        }else if(view == btnMiss){
            listener.onSalutationSelected(getString(R.string.salutations_miss));
        }else if(view == btnMisis){
            listener.onSalutationSelected(getString(R.string.salutations_misis));
        }
    }

    public void setListener(OnSalutationSelectedListener listener) {
        this.listener = listener;
    }

    public interface OnSalutationSelectedListener {
        public void onSalutationSelected(String salutation);
    }
}
