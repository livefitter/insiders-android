package com.livefitter.theinsiders.view;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.livefitter.theinsiders.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

/**
 * Created by LFT-PC-010 on 7/20/2017.
 */

public class ExpirationDateDialogFragment extends AppCompatDialogFragment {

    private static Calendar mCalendar = Calendar.getInstance();
    private static OnExpiryDateSelectListener mListener;

    private AppCompatSpinner spMonth, spYear;

    private ArrayList<String> arrayMonth = new ArrayList<>();
    private ArrayList<String> arrayYear = new ArrayList<>();

    private static final int MAX_YEAR_COUNT = 15;

    public static AppCompatDialogFragment Create(int month, int year, OnExpiryDateSelectListener listener){

        mCalendar.set(Calendar.MONTH, month);
        mCalendar.set(Calendar.YEAR, year);

        mListener = listener;

        return new ExpirationDateDialogFragment();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        View rootView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_expiry_date, null);

        spMonth = (AppCompatSpinner) rootView.findViewById(R.id.check_out_spinner_month);
        spYear  = (AppCompatSpinner) rootView.findViewById(R.id.check_out_spinner_year);

        populateArrays();

        spMonth.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, arrayMonth));
        spYear.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, arrayYear));

        spMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mCalendar.set(Calendar.MONTH, position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mCalendar.set(Calendar.YEAR, Integer.valueOf(arrayYear.get(position)));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spMonth.setSelection(mCalendar.get(Calendar.MONTH));
        spYear.setSelection(arrayYear.indexOf(String.valueOf(mCalendar.get(Calendar.YEAR))));

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setView(rootView);
        alertDialog.setTitle(getActivity().getString(R.string.label_expiry_date));
        alertDialog.setNegativeButton(getActivity().getString(R.string.label_cancel), null);
        alertDialog.setPositiveButton(getActivity().getString(R.string.label_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if(which == DialogInterface.BUTTON_POSITIVE){

                    mListener.onDateSelect(mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.YEAR));

                }

            }
        });


        return alertDialog.create();
    }

    private void populateArrays(){

        arrayMonth = new ArrayList<String>(Arrays.asList(getActivity().getResources().getStringArray(R.array.array_month)));

        Calendar calendar = Calendar.getInstance();
        arrayYear.clear();

        for(int ctr = 0; ctr < MAX_YEAR_COUNT; ctr ++){

            arrayYear.add(String.valueOf(calendar.get(Calendar.YEAR)));

            calendar.add(Calendar.YEAR, 1);

        }

    }

    public interface OnExpiryDateSelectListener {

        void onDateSelect(int month, int year);

    }
}
