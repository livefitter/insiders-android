package com.livefitter.theinsiders.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatSeekBar;
import android.util.AttributeSet;
import android.view.MotionEvent;


/**
 * Created by LloydM on 3/9/17
 * for Livefitter
 * <p>
 * see: http://stackoverflow.com/a/22410875
 */

public class SlideButton extends AppCompatSeekBar {

    private static final int THRESHOLD_VALUE = 95;

    private Drawable thumb;
    private SlideButtonListener listener;
    /**
     * Flag that prevents onSlideEnd being called more than once per ACTION_DOWN -> ACTION_MOVE
     */
    private boolean hasReachedThreshold = false;

    public SlideButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setThumb(Drawable thumb) {
        super.setThumb(thumb);
        this.thumb = thumb;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (thumb.getBounds().contains((int) event.getX(), (int) event.getY())) {

                if (listener != null) {
                    listener.onSlideStart();
                }

                hasReachedThreshold = false;

                super.onTouchEvent(event);
            } else {
                return false;
            }
        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            if (getProgress() > THRESHOLD_VALUE) {
                if (listener != null && !hasReachedThreshold) {
                    listener.onSlideEnd();
                }

                hasReachedThreshold = true;

                return false;
            } else {
                super.onTouchEvent(event);
            }
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            if (listener != null) {
                listener.onSlideRelease();
            }

            hasReachedThreshold = false;

            setProgress(0);
        } else
            super.onTouchEvent(event);

        return true;
    }

    public void setSlideButtonListener(SlideButtonListener listener) {
        this.listener = listener;
    }

    public interface SlideButtonListener {
        public void onSlideStart();

        public void onSlideRelease();

        public void onSlideEnd();
    }
}
