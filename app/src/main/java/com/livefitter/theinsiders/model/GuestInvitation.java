package com.livefitter.theinsiders.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.android.databinding.library.baseAdapters.BR;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;


/**
 * Created by LloydM on 3/6/17
 * for Livefitter
 */

public class GuestInvitation extends BaseObservable implements Parcelable, Cloneable {
    /*"address": "Manila",
    "date_time": "01 June 2017, 07:00 PM",
    "event_image_url": "http://theinsiders.s3-us-west-2.amazonaws.com/events/images/000/000/011/original/Floral_Handbag_1.png?1488361417",
    "invite_description": "You are invited",
    "invited_by": "Joseph Lloyd",
    "name": "Jeri Miah",
    "qr_code_url": "http://api.qrserver.com/v1/create-qr-code/?data=JeriMiah,11&size=150x150",
    "salutation": "Ms",
    "title": "EEE Event"*/

    private String address = "";
    @SerializedName("date_time")
    private String dateTime = "";
    @SerializedName("event_image_url")
    private String eventImageUrl = "";
    @SerializedName("invite_description")
    private String inviteDescription = "";
    @SerializedName("invited_by")
    private String invitedBy = "";
    private String name = "";
    @SerializedName("qr_code_url")
    private String qrCodeUrl = "";
    private String salutation = "";
    private String title = "";

    private GuestInvitation(String address, String dateTime, String eventImageUrl, String inviteDescription, String invitedBy, String name, String qrCodeUrl, String salutation, String title) {
        this.address = address;
        this.dateTime = dateTime;
        this.eventImageUrl = eventImageUrl;
        this.inviteDescription = inviteDescription;
        this.invitedBy = invitedBy;
        this.name = name;
        this.qrCodeUrl = qrCodeUrl;
        this.salutation = salutation;
        this.title = title;
    }

    public GuestInvitation(Parcel in){
        readFromParcel(in);
    }

    public static final Creator<GuestInvitation> CREATOR = new Creator<GuestInvitation>() {
        @Override
        public GuestInvitation createFromParcel(Parcel in) {
            return new GuestInvitation(in);
        }

        @Override
        public GuestInvitation[] newArray(int size) {
            return new GuestInvitation[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    private void readFromParcel(Parcel in) {
        address = in.readString();
        dateTime = in.readString();
        eventImageUrl = in.readString();
        inviteDescription = in.readString();
        invitedBy = in.readString();
        name = in.readString();
        qrCodeUrl = in.readString();
        salutation = in.readString();
        title = in.readString();
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeString(address);
        out.writeString(dateTime);
        out.writeString(eventImageUrl);
        out.writeString(inviteDescription);
        out.writeString(invitedBy);
        out.writeString(name);
        out.writeString(qrCodeUrl);
        out.writeString(salutation);
        out.writeString(title);
    }

    @Bindable
    public String getAddress() {
        return address;
    }

    @Bindable
    public String getDateTime() {
        return dateTime;
    }

    @Bindable
    public String getEventImageUrl() {
        return eventImageUrl;
    }

    @Bindable
    public String getInviteDescription() {
        return inviteDescription;
    }

    @Bindable
    public String getInvitedBy() {
        return invitedBy;
    }

    @Bindable
    public String getName() {
        return name;
    }

    @Bindable
    public String getQrCodeUrl() {
        return qrCodeUrl;
    }

    @Bindable
    public String getSalutation() {
        return salutation;
    }

    @Bindable
    public String getTitle() {
        return title;
    }

    public void setAddress(String address) {
        this.address = address;
        notifyPropertyChanged(BR.address);
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
        notifyPropertyChanged(BR.dateTime);
    }

    public void setEventImageUrl(String eventImageUrl) {
        this.eventImageUrl = eventImageUrl;
        notifyPropertyChanged(BR.eventImageUrl);
    }

    public void setInviteDescription(String inviteDescription) {
        this.inviteDescription = inviteDescription;
        notifyPropertyChanged(BR.inviteDescription);
    }

    public void setInvitedBy(String invitedBy) {
        this.invitedBy = invitedBy;
        notifyPropertyChanged(BR.invitedBy);
    }

    public void setName(String name) {
        this.name = name;
        notifyPropertyChanged(BR.name);
    }

    public void setQrCodeUrl(String qrCodeUrl) {
        this.qrCodeUrl = qrCodeUrl;
        notifyPropertyChanged(BR.qrCodeUrl);
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
        notifyPropertyChanged(BR.salutation);
    }

    public void setTitle(String title) {
        this.title = title;
        notifyPropertyChanged(BR.title);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        return new GuestInvitation(this.address,
                this.dateTime,
                this.eventImageUrl,
                this.inviteDescription,
                this.invitedBy,
                this.name,
                this.qrCodeUrl,
                this.salutation,
                this.title);
    }

    public GuestInvitation newInstance(){
        try {
            return (GuestInvitation)this.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public String toString() {
        String fieldDeclarationString = this.getClass().getSimpleName() + "{ ";
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field f : fields) {
            try {
                if (f.getModifiers() == Modifier.PRIVATE && !f.getName().equals("CREATOR")) {
                    fieldDeclarationString += f.getName() + ": " + f.get(this) + ", \n";
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        fieldDeclarationString += "}";

        return fieldDeclarationString;
    }
}
