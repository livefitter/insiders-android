package com.livefitter.theinsiders.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;


/**
 * Created by LloydM on 12/1/17
 * for Livefitter
 */

public class PromoCode implements Parcelable {

    /**
     * discount : 10
     * id : 1
     */

    private int discount;
    private int id;

    protected PromoCode(Parcel in) {
        discount = in.readInt();
        id = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(discount);
        dest.writeInt(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PromoCode> CREATOR = new Creator<PromoCode>() {
        @Override
        public PromoCode createFromParcel(Parcel in) {
            return new PromoCode(in);
        }

        @Override
        public PromoCode[] newArray(int size) {
            return new PromoCode[size];
        }
    };

    public int getDiscount() {
        return discount;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
