package com.livefitter.theinsiders.utility;

import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;

import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.activity.FaqActivity;
import com.livefitter.theinsiders.activity.TermsActivity;

/**
 * Created by LFT-PC-010 on 6/23/2017.
 */

public class FaqTermsUtil {

    public static void setSpannableContent(final BaseActivity mActivity, TextView textView){

        String part1 = mActivity.getString(R.string.label_faq_terms_part_1);
        String part2 = mActivity.getString(R.string.label_faq_terms_part_2);
        String part3 = mActivity.getString(R.string.label_faq_terms_part_3);

        String terms = mActivity.getString(R.string.label_terms_of_service);

        String faqs = mActivity.getString(R.string.label_faqs);

        SpannableString termsLink = new SpannableString(terms);
        termsLink.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {

                mActivity.switchActivity(TermsActivity.class, false);

            }
        }, 0, terms.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        termsLink.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mActivity, R.color.text_gold)), 0, terms.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        SpannableString faqsLink = new SpannableString(faqs);
        faqsLink.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {

                mActivity.switchActivity(FaqActivity.class, false);

            }
        }, 0, faqs.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        faqsLink.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mActivity, R.color.text_gold)), 0, faqs.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView.append(part1);
        textView.append(termsLink);
        textView.append(part2);
        textView.append(faqsLink);
        textView.append(part3);

        MovementMethod mm = textView.getMovementMethod();
        if ((mm == null) || !(mm instanceof LinkMovementMethod)) {
            if (textView.getLinksClickable()) {
                textView.setMovementMethod(LinkMovementMethod.getInstance());
            }
        }

    }

}
