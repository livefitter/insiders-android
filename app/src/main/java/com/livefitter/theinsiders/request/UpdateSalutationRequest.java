package com.livefitter.theinsiders.request;

import android.content.Context;

import com.livefitter.theinsiders.AppConstants;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Callback;
import okhttp3.RequestBody;

/**
 * Created by LloydM on 2/6/17
 * for Livefitter
 */

public class UpdateSalutationRequest extends BaseRequest {
    private int mUserId;
    private String mSalutation;

    public UpdateSalutationRequest(Context context, int userId, String salutation, Callback requestCallback) {
        super(context, true, requestCallback);
        this.mUserId = userId;
        this.mSalutation = salutation;
    }

    @Override
    protected void buildRequest() {

        String url = AppConstants.urlUpdateSalutation() + "/" + mUserId;

        JSONObject jsonParams = null;
        try {
            jsonParams = generateJsonParams();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(jsonParams != null) {
            RequestBody body = RequestBody.create(MEDIATYPE_JSON, jsonParams.toString());

            mRequestBuilder.url(url)
                    .put(body);

            mRequest = mRequestBuilder.build();
        }
    }

    private JSONObject generateJsonParams() throws JSONException {
        JSONObject jsonParams = new JSONObject();
        JSONObject dataJson = new JSONObject();

        dataJson.put("salutation", mSalutation);
        jsonParams.put("data", dataJson);

        return jsonParams;
    }

}
