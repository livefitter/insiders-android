package com.livefitter.theinsiders.request;

import android.content.Context;

import com.livefitter.theinsiders.AppConstants;

import okhttp3.Callback;

/**
 * Created by LloydM on 2/6/17
 * for Livefitter
 */

public class LogOutRequest extends BaseRequest {
    private int mUserId;

    public LogOutRequest(Context context, int userId, Callback requestCallback) {
        super(context, true, requestCallback);
        this.mUserId = userId;
    }

    @Override
    protected void buildRequest() {

        String url = AppConstants.urlLogOut() + "/" + mUserId;

        mRequestBuilder.url(url)
                .delete();

        mRequest = mRequestBuilder.build();
    }
}
