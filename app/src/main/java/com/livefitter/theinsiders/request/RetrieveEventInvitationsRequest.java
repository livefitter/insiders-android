package com.livefitter.theinsiders.request;

import android.content.Context;

import com.livefitter.theinsiders.AppConstants;

import okhttp3.Callback;

/**
 * Created by LloydM on 2/22/17
 * for Livefitter
 */

public class RetrieveEventInvitationsRequest extends BaseRequest {

    private int mEventId;

    public RetrieveEventInvitationsRequest(Context context, int eventId, Callback requestCallback) {
        super(context, true, requestCallback);

        this.mEventId = eventId;
    }

    @Override
    protected void buildRequest() {
        String url = AppConstants.urlEventInvitations() + "/" + mEventId;

        mRequestBuilder.url(url);

        mRequest = mRequestBuilder.build();
    }
}
