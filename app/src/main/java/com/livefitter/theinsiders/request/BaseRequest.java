package com.livefitter.theinsiders.request;

import android.content.Context;
import android.content.SharedPreferences;

import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.utility.SharedPreferencesHelper;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by LloydM on 2/2/17
 * for Livefitter
 */

public abstract class BaseRequest{

    public static final MediaType MEDIATYPE_JSON = MediaType.parse("application/json; charset=utf-8");

    protected boolean mUseAuthentication = false;

    protected Context context;
    protected OkHttpClient mClient;
    protected Call mCall;
    protected Request mRequest;
    protected Response mResponse;
    protected Request.Builder mRequestBuilder;

    protected Callback mCallback;

    public BaseRequest(Context context, boolean useAuthentication, Callback requestCallback){
        this.context = context;
        this.mUseAuthentication = useAuthentication;
        this.mCallback = requestCallback;

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.retryOnConnectionFailure(true);
        builder.addNetworkInterceptor(new LoggingInterceptor());
        mClient = builder.build();

        mRequestBuilder = new Request.Builder();
    }

    /**
     * Optional method that applies the authorization to the request header
     * if this request needs it
     */
    protected void applyAuthentication(){
        if(mUseAuthentication){
            String authToken = SharedPreferencesHelper.getAuthToken(context);

            if(!authToken.isEmpty()) {
                mRequestBuilder.header("Authorization", authToken);
            }
        }
    }

    /**
     * Cancels the request call
     */
    public void cancelCall(){
        if(mCall != null && mCall.isExecuted()){
            mCall.cancel();
        }
    }

    public OkHttpClient getHttpClient(){
        return mClient;
    }

    /**
     * Implementing classes should build their request object here.
     * Use the {@link BaseRequest#mRequestBuilder Request builder} field.
     */
    protected abstract void buildRequest();

    /**
     * Execute the request call
     */
    public void execute(){
        applyAuthentication();
        buildRequest();

        if(mClient == null
                || mRequest == null
                || mCallback == null){
            return;
        }

        // Safety net to cancel the call if one is already running
        cancelCall();

        mCall = mClient.newCall(mRequest);
        mCall.enqueue(mCallback);
    };
}
