package com.livefitter.theinsiders.request;

import android.content.Context;

import com.livefitter.theinsiders.AppConstants;

import okhttp3.Callback;

/**
 * Created by LFT-PC-010 on 6/16/2017.
 */

public class ViewNotificationRequest extends BaseRequest {

    private int notificationId;

    public ViewNotificationRequest(Context context, int notificationId, Callback requestCallback) {
        super(context, true, requestCallback);

        this.notificationId = notificationId;

    }

    @Override
    protected void buildRequest() {

        String url = AppConstants.urlNotificationList() + "/" + notificationId;

        mRequestBuilder.url(url).get();

        mRequest = mRequestBuilder.build();


    }
}
