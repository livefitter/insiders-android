package com.livefitter.theinsiders.request;

import android.content.Context;

import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.model.Guest;
import com.livefitter.theinsiders.model.PromoCode;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.Callback;
import okhttp3.RequestBody;

/**
 *
 * RSVP and guest invite requests are rolled into this one request as they use almost identical
 * Created by LloydM on 2/16/17
 * for Livefitter
 */

public class RsvpGuestsRequest extends BaseRequest {

    private int mEventId, quantity;
    private ArrayList<Guest> mGuestList;
    private boolean rsvpUser;
    private String cardToken;
    private PromoCode promoCode;

    //for normal event
    public RsvpGuestsRequest(Context context, int eventId, ArrayList<Guest> guestList, boolean rsvpUser, Callback requestCallback) {
        super(context, true, requestCallback);

        this.mEventId = eventId;
        this.mGuestList = guestList;
        this.rsvpUser = rsvpUser;
    }

    //for paid event
    public RsvpGuestsRequest(Context context, int eventId, ArrayList<Guest> guestList, boolean rsvpUser, int quantity, String cardToken, PromoCode promoCode, Callback requestCallback) {
        super(context, true, requestCallback);

        this.mEventId = eventId;
        this.mGuestList = guestList;
        this.rsvpUser = rsvpUser;
        this.quantity = quantity;
        this.cardToken = cardToken;
        this.promoCode = promoCode;

    }

    @Override
    protected void buildRequest() {

        String url = rsvpUser? AppConstants.urlRsvp() : AppConstants.urlInviteGuests();

        JSONObject jsonParams = null;
        try {
            jsonParams = generateJsonParams();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        if(jsonParams != null) {
            RequestBody body = RequestBody.create(MEDIATYPE_JSON, jsonParams.toString());

            mRequestBuilder.url(url)
                    .post(body);

            mRequest = mRequestBuilder.build();
        }
    }

    private JSONObject generateJsonParams() throws JSONException {
        JSONObject jsonParams = new JSONObject();
        JSONObject dataJson = new JSONObject();

        dataJson.put("event_id", mEventId);

        JSONArray guestJsonArray = new JSONArray();
        if(mGuestList != null && !mGuestList.isEmpty()){
            for(int i = 0; i < mGuestList.size(); i++) {
                Guest guest = mGuestList.get(i);

                JSONObject guestJson = new JSONObject();

                guestJson.put("name", guest.getName());
                guestJson.put("salutation", guest.getSalutation());

                guestJsonArray.put(guestJson);
            }
        }

        //for paid events parameters
        if(quantity > 0 && cardToken != null) {

            dataJson.put("qty", quantity);

            dataJson.put("stripe_token", cardToken);

            // Optional if using promo code
            if (promoCode != null) {
                dataJson.put("promo_code_id", promoCode.getId());
            }
        }

        dataJson.put("guests", guestJsonArray);

        jsonParams.put("data", dataJson);

        return jsonParams;
    }
}
