package com.livefitter.theinsiders.request;

import android.content.Context;

import com.livefitter.theinsiders.AppConstants;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Callback;
import okhttp3.RequestBody;

/**
 * Created by LloydM on 2/9/17
 * for Livefitter
 */

public class VerifyActivationCodeRequest extends BaseRequest {
    private String mActivationCode;

    public VerifyActivationCodeRequest(Context context, String activationCode, Callback requestCallback) {
        super(context, false, requestCallback);

        this.mActivationCode = activationCode;
    }

    @Override
    protected void buildRequest() {

        String url = AppConstants.urlVerifyActivationCode();

        JSONObject jsonParams = null;
        try {
            jsonParams = generateJsonParams();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(jsonParams != null) {
            RequestBody body = RequestBody.create(MEDIATYPE_JSON, jsonParams.toString());

            mRequestBuilder.url(url)
                    .post(body);

            mRequest = mRequestBuilder.build();
        }
    }

    private JSONObject generateJsonParams() throws JSONException {
        JSONObject jsonParams = new JSONObject();
        JSONObject dataJson = new JSONObject();

        dataJson.put("activation_code", mActivationCode);
        jsonParams.put("data", dataJson);

        return jsonParams;
    }
}
