package com.livefitter.theinsiders.request;

import android.content.Context;

import com.livefitter.theinsiders.AppConstants;

import okhttp3.Callback;

/**
 * Created by LFT-PC-010 on 6/20/2017.
 */

public class RemoveInterestRequest extends BaseRequest {

    private int eventId;

    public RemoveInterestRequest(Context context, int eventId, Callback requestCallback) {
        super(context, true, requestCallback);
        this.eventId = eventId;
    }

    @Override
    protected void buildRequest() {

        String url = AppConstants.urlRsvp() + "/" + eventId;

        mRequestBuilder.url(url)
                .delete();

        mRequest = mRequestBuilder.build();
    }

}
