package com.livefitter.theinsiders.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.activity.MainActivity;
import com.livefitter.theinsiders.controller.SupportController;

/**
 * Created by LFT-PC-010 on 6/21/2017.
 */

public class SupportFragment extends BaseFragment {

    private SupportController mController;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mController = new SupportController(((MainActivity) getActivity()), this);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        mController.onCreateOptionsMenu(menu, inflater);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_support, container, false);

        mController.initialize(rootView, savedInstanceState);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        AppConstants.FRAG_CURRENT = AppConstants.FRAG_SUPPORT;
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }
}
