package com.livefitter.theinsiders.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.controller.SignUpController;


/**
 * Created by LloydM on 2/9/17
 * for Livefitter
 */

public class SignUpFragment extends BaseFragment {

    private SignUpController mController;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        if(getArguments() != null) {
            Bundle arguments = getArguments();
            String activationCode = arguments.getString(AppConstants.KEY_ACTIVATION_CODE);

            mController = new SignUpController((BaseActivity) getActivity(), this);
            mController.setActivationCode(activationCode);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sign_up, container, false);

        mController.initialize(rootView, savedInstanceState);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        AppConstants.FRAG_CURRENT = AppConstants.FRAG_SIGN_UP;
    }

    @Override
    public void onStop() {
        super.onStop();
        if(mController != null){
            mController.onStop();
        }
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }
}
