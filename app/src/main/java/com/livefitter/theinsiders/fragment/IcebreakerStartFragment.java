package com.livefitter.theinsiders.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.controller.IcebreakerStartController;
import com.livefitter.theinsiders.databinding.FragmentIcebreakerStartBinding;


/**
 * Created by LloydM on 3/3/17
 * for Livefitter
 */

public class IcebreakerStartFragment extends BaseFragment {

    IcebreakerStartController mController;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);

        mController = new IcebreakerStartController((BaseActivity) getActivity(), this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentIcebreakerStartBinding binding = FragmentIcebreakerStartBinding.inflate(inflater, container, false);

        View rootView = binding.getRoot();
        mController.setBinding(binding);
        mController.initialize(rootView, savedInstanceState);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        mController.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onResume() {
        super.onResume();
        AppConstants.FRAG_CURRENT = AppConstants.FRAG_ICEBREAKER;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mController.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onStop() {
        super.onStop();
        if(mController != null){
            mController.onStop();
        }
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }
}
