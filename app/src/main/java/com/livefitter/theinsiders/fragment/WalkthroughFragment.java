package com.livefitter.theinsiders.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.adapter.WalkthroughPagerAdapter;
import com.livefitter.theinsiders.utility.SharedPreferencesHelper;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

/**
 * Created by LFT-PC-010 on 7/10/2017.
 */

public class WalkthroughFragment extends BaseFragment {

    private ViewPager mViewPager;
    private CirclePageIndicator mPageIndicator;
    private TextView btnSkip;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_walkthrough, container, false);

        initialize(rootView);

        return rootView;
    }

    private void initialize(View rootView) {

        mViewPager      = (ViewPager) rootView.findViewById(R.id.viewpager);
        mPageIndicator  = (CirclePageIndicator) rootView.findViewById(R.id.page_indicator);
        btnSkip         = (TextView) rootView.findViewById(R.id.btn_skip);

        mViewPager.setPageTransformer(false, new CrossFadeTransformer());
        mViewPager.setAdapter(setupAdapter());

        mPageIndicator.setPageColor(ContextCompat.getColor(getActivity(), R.color.dark_gray));
        mPageIndicator.setFillColor(ContextCompat.getColor(getActivity(), R.color.gold));
        mPageIndicator.setStrokeColor(ContextCompat.getColor(getActivity(), R.color.dark_gray));

        mPageIndicator.setViewPager(mViewPager);

        mPageIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if(position == (mViewPager.getAdapter().getCount() - 1)){

                    btnSkip.setText(getActivity().getString(R.string.label_done));

                }
                else{

                    btnSkip.setText(getActivity().getString(R.string.label_skip));

                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferencesHelper.setShouldDisplayWalkThrough(getActivity(), false);
                getActivity().finish();

            }
        });

    }

    private WalkthroughPagerAdapter setupAdapter(){

        int[] images = {R.drawable.img_walkthrough_1,
                        R.drawable.img_walkthrough_2,
                        R.drawable.img_walkthrough_3,
                        R.drawable.img_walkthrough_4,
                        R.drawable.img_walkthrough_5,
                        R.drawable.img_walkthrough_6};

        ArrayList<Fragment> children = new ArrayList<>();

        for(int ctr = 0; ctr < images.length; ctr ++){

            Bundle bundle = new Bundle();
            bundle.putInt(AppConstants.KEY_IMAGE_RESOURCE, images[ctr]);

            Fragment child = new WalkthroughItemFragment();
            child.setArguments(bundle);

            children.add(child);

        }

        return new WalkthroughPagerAdapter(getChildFragmentManager(), children);

    }

    private class CrossFadeTransformer implements ViewPager.PageTransformer {

        @Override
        public void transformPage(View view, float position) {

            view.setTranslationX(view.getWidth() * -position);

            if(position <= -1.0F || position >= 1.0F) {
                view.setAlpha(0.0F);
            } else if( position == 0.0F ) {
                view.setAlpha(1.0F);
            } else {
                // position is between -1.0F & 0.0F OR 0.0F & 1.0F
                view.setAlpha(1.0F - Math.abs(position));
            }

        }

    }

    @Override
    public boolean onBackPressed() {
        return true;
    }
}
