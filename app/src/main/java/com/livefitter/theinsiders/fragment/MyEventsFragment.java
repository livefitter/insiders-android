package com.livefitter.theinsiders.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.controller.MyEventsController;


/**
 * Created by LloydM on 3/7/17
 * for Livefitter
 */

public class MyEventsFragment extends BaseFragment {

    MyEventsController mController;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        mController = new MyEventsController((BaseActivity)getActivity(), this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_my_events, container, false);

        mController.initialize(rootView, savedInstanceState);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        mController.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onResume() {
        super.onResume();
        AppConstants.FRAG_CURRENT = AppConstants.FRAG_MY_EVENTS;
    }

    @Override
    public void onStop() {
        super.onStop();
        if(mController != null){
            mController.onStop();
        }
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }
}
