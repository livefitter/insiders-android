package com.livefitter.theinsiders.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.controller.ChangePasswordController;


/**
 * Created by LloydM on 3/3/17
 * for Livefitter
 */

public class ChangePasswordFragment extends BaseFragment {

    ChangePasswordController mController;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        mController = new ChangePasswordController((BaseActivity) getActivity(), this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_change_password, container, false);
        mController.initialize(rootView, savedInstanceState);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        mController.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        switch (itemId){
            case android.R.id.home:
                getActivity().finish();
                return false;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStop() {
        super.onStop();
        if(mController != null){
            mController.onStop();
        }
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }
}
