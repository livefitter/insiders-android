package com.livefitter.theinsiders.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.controller.ActivationController;
import com.livefitter.theinsiders.listener.LinkedInSessionListener;


/**
 * Created by LloydM on 2/6/17
 * for Livefitter
 */

public class ActivationFragment extends BaseFragment
        implements LinkedInSessionListener {

    private ActivationController mController;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mController = new ActivationController((BaseActivity) getActivity(), this);
    }

    @Override
    public void onResume() {
        super.onResume();
        AppConstants.FRAG_CURRENT = AppConstants.FRAG_ACTIVATION;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_activation, container, false);

        mController.initialize(rootView, savedInstanceState);

        return rootView;
    }

    @Override
    public void onStop() {
        super.onStop();
        if(mController != null){
            mController.onStop();
        }
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }

    @Override
    public void onLinkedInSessionReady() {
        mController.onLinkedInSessionReady();
    }

    @Override
    public void onLinkedInSessionError(String message) {
        mController.onLinkedInSessionError(message);
    }
}
