package com.livefitter.theinsiders.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.controller.AddCreditCardController;

/**
 * Created by LFT-PC-010 on 7/5/2017.
 */

public class AddCreditCardFragment extends BaseFragment {

    private AddCreditCardController mController;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        mController = new AddCreditCardController((BaseActivity) getActivity(), this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        mController.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case android.R.id.home:{
                getActivity().onBackPressed();
                return true;
            }
            default:{
                return super.onOptionsItemSelected(item);
            }

        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_credit_card, container, false);

        mController.initialize(rootView, savedInstanceState);

        return rootView;

    }

    @Override
    public void onResume() {
        super.onResume();
        AppConstants.FRAG_CURRENT = AppConstants.FRAG_ADD_CREDIT_CARD;
    }

    @Override
    public boolean onBackPressed() {
        //check if checkout mode
        boolean isCheckoutMode = false;
        if(getArguments() != null) {
            isCheckoutMode = getArguments().getBoolean(AppConstants.KEY_CHECK_OUT_MODE, false);
        }

        //if checkoutMode, it means that this fragment was called from PriceSummaryFragment and let it handle its own backPressed
        if(isCheckoutMode){
            getActivity().getSupportFragmentManager().popBackStackImmediate();
            return false;
        }

        return true;
    }
}
