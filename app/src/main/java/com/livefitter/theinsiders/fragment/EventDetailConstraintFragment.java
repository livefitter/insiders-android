package com.livefitter.theinsiders.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.controller.EventDetailConstraintController;
import com.livefitter.theinsiders.databinding.FragmentEventDetailConstraintBinding;


/**
 * Created by LloydM on 2/23/17
 * for Livefitter
 */

public class EventDetailConstraintFragment extends BaseFragment{

    private EventDetailConstraintController mController;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        mController = new EventDetailConstraintController((BaseActivity) getActivity(), this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentEventDetailConstraintBinding binding = FragmentEventDetailConstraintBinding.inflate(inflater, container, false);
        // This is important to be called before mController.initialize()
        // as we need the binding to be initialized prior to any other logic affecting the view.
        mController.setBinding(binding);

        View rootView = binding.getRoot();

        mController.initialize(rootView, savedInstanceState);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        mController.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        switch (itemId){
            case android.R.id.home:
                getActivity().finish();
                return false;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        AppConstants.FRAG_CURRENT = AppConstants.FRAG_EVENT_DETAIL;

        mController.retrieveFullEventDetails();
    }

    @Override
    public void onStop() {
        super.onStop();
        if(mController != null){
            mController.onStop();
        }
    }

    @Override
    public boolean onBackPressed() {
        return mController.onBackPressed();
    }


}
